
def jumpgame(y, x):
	if y >= nSize or x >= nSize:
		return False
	if board[y][x] == 0:
		return True
	if cached[y][x] is not None:
		return cached[y][x]
	ret = False
	ret = jumpgame(y + board[y][x], x) or jumpgame(y, x + board[y][x])
	cached[y][x] =ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nSize = int(rl().strip())
	board = []
	for i in range(nSize):
		oneLine = [int(i) for i in rl().strip().split(' ')]
		board.append(oneLine)
	cached = [[None for i in range(nSize)] for j in range(nSize)]
	ans = jumpgame(0, 0)
	if ans:
		print "YES"
	else:
		print "NO"
