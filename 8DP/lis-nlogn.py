
def lis(nums):
	from bisect import bisect_left
	C = [float('inf') for i in range(lenNums)]
	C[0] = nums[0]
	for eachNum in nums[1:]:
		largestLessThanInd = bisect_left(C, eachNum)-1
		if C[largestLessThanInd+1] > eachNum: C[largestLessThanInd+1] = eachNum
	for iInd, i in enumerate(reversed(C)):
		if i!=float('inf'):
			return len(C)-iInd
import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenNums = int(rl().strip())
	nums = [int(i) for i in rl().strip().split(' ')]
	print lis(nums)
