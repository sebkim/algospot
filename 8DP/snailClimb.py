
def snailClimb(combi, mDays, targetClimb, sunnyProb):
	if len(combi) == mDays:
		if sum(combi) >= targetClimb:
			return 1
		else:
			return 0
	ret = (1 - sunnyProb)*snailClimb(combi + [1], mDays, targetClimb, sunnyProb) +\
	 (sunnyProb)*snailClimb(combi + [2], mDays, targetClimb, sunnyProb)
	return ret

mDays = 23
targetClimb = 34
# print snailClimb([], mDays, targetClimb, 0.5)


cached = [[None for i in range(2*mDays)] for j in range(mDays)]
def snailClimb2(days, climbed):
	if days==mDays:
		if climbed >= targetClimb:
			return 1
		else:
			return 0
	if cached[days][climbed] is not None:
		return cached[days][climbed]
	ret = snailClimb2(days+1, climbed+1) + snailClimb2(days+1, climbed+2)
	cached[days][climbed] = ret
	return ret

print snailClimb2(0, 0) / float(pow(2, mDays))
# print snailClimb([], mDays, targetClimb, 0.5)