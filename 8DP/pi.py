
def classify(a, b):
	subPIs = pis[a:b+1]
	firstChar = subPIs[0]
	if all(i==firstChar for i in subPIs[1:]):
		return 1

	boolMonotone = True
	for i in range(len(subPIs[:-1])):
		if int(subPIs[i+1]) - int(subPIs[i]) != int(subPIs[1]) - int(subPIs[0]):
			boolMonotone = False
			break
	if boolMonotone and abs(int(subPIs[1]) - int(subPIs[0])) == 1:
		return 2

	boolAlternating = True
	for i in range(len(subPIs)):
		if int(subPIs[i]) != int(subPIs[i%2]):
			boolAlternating = False
			break
	if boolAlternating:
		return 4
	if boolMonotone:
		return 5
	return 10
	 
def pi(begin):
	if begin == len(pis):
		return 0
	if cached[begin] is not None:
		return cached[begin]
	ret = float('inf')
	for size in range(3,6):
		if begin+size <= len(pis):
			ret = min(ret, pi(begin+size) + classify(begin, begin+size-1))
	cached[begin] = ret
	return ret

import sys
sys.setrecursionlimit(10000)
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	pis = rl().strip()
	cached = [None for i in range(len(pis))]
	print( pi(0) )


