
def lis(start):
	if start==lenNums-1:
		cached[start] = 1
		return 1
	if cached[start] is not None:
		return cached[start]
	ret = 1
	for next in range(start+1, lenNums):
		if nums[start] < nums[next]:
			# ret = max(ret, lis(next)+1)
			nextRet = lis(next)+1
			if ret < nextRet:
				ret = nextRet
				bestNext[start] = next
	cached[start] = ret
	return ret
def reconstruct(start, reconList):
	next = bestNext[start]
	reconList.append(nums[start])
	if next is not None:
		reconstruct(next, reconList)
	else:
		print reconList

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenNums = int(rl().strip())
	nums = [int(i) for i in rl().strip().split(' ')]
	cached = [None for i in range(lenNums)]
	bestNext = [None for i in range(lenNums)]
	ans = 0
	for eachStart in range(lenNums):
		# ans = max(ans, lis(eachStart))
		nextAns = lis(eachStart)
		if ans < nextAns:
			ans = nextAns
			bestNextInit = eachStart
	print ans
	print nums
	reconstruct(bestNextInit, [])

# nums = [5,1,2]
# lenNums = len(nums)
# cached = [None for i in range(lenNums)]
# print lis(1)