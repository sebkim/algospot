
def classify(a, b):
	subPIs = pis[a:b+1]
	firstChar = subPIs[0]
	if all(i==firstChar for i in subPIs[1:]):
		return 1

	boolMonotone = True
	for i in range(len(subPIs[:-1])):
		if int(subPIs[i+1]) - int(subPIs[i]) != int(subPIs[1]) - int(subPIs[0]):
			boolMonotone = False
			break
	if boolMonotone and abs(int(subPIs[1]) - int(subPIs[0])) == 1:
		return 2

	boolAlternating = True
	for i in range(len(subPIs)):
		if int(subPIs[i]) != int(subPIs[i%2]):
			boolAlternating = False
			break
	if boolAlternating:
		return 4
	if boolMonotone:
		return 5
	return 10
	 
def pi(begin):
	ans = [None for i in range(len(pis)+1)]
	ans[-2] = ans[-3] = float('inf')
	ans[-1] = 0
	for i in range(len(ans)-4, -1, -1):
		cand = float('inf')
		for size in range(3,6):
			if i+size-1 < len(pis):
				cand = min(cand, classify(i, i+size-1) + ans[i+size])
		ans[i] = cand
	return ans[0]
				
import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	pis = rl().strip()
	print( pi(0) )


