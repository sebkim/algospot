
def maxPathSum(y, x):
	if y==len(triangle):
		return 0
	if cached[y][x] is not None:
		return cached[y][x]
	ret = max(triangle[y][x]+maxPathSum(y+1, x), triangle[y][x]+maxPathSum(y+1, x+1))
	cached[y][x] = ret
	return ret
def count(y, x):
	if y==len(triangle)-1:
		cached2[y][x] = 1
		return 1
	if cached2[y][x] is not None:
		return cached2[y][x]
	if cached[y+1][x] > cached[y+1][x+1]:
		ret = count(y+1, x)
	elif cached[y+1][x] < cached[y+1][x+1]:
		ret = count(y+1, x+1)
	else:
		ret = count(y+1, x+1) + count(y+1, x)
	cached2[y][x] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	height = int(rl().strip())
	triangle = []
	for h in range(height):
		nums = [ int(i) for i in rl().strip().split() ]
		triangle.append(nums)
	cached = [[None for i in range(len(triangle[-1]))] for j in range(len(triangle))]
	maxPathSum(0, 0)
	cached2 = [[None for i in range(len(triangle[-1]))] for j in range(len(triangle))]
	print count(0, 0)
	