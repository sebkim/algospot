//
//  main.cpp
//  c-jumpgame
//
//  Created by Sebo Kim on 11/1/16.
//  Copyright © 2016 Sebo Kim. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

int nSize;
vector<vector<int> > board;
vector<vector<int> > cached;

int jumpgame(int y, int x)
{
    if ( y>= nSize || x>= nSize) return 0;
    if (board[y][x] == 0) return 1;
    int &ret = cached[y][x];
    if (ret != -1) return ret;
    ret = 0;
    ret = jumpgame(y+board[y][x], x) || jumpgame(y, x+board[y][x]);
    return ret;
}

int main() {
    ios::sync_with_stdio(false);
    int cases;
    cin>>cases;
     while(cases--) {
         cin>>nSize;
         board = vector<vector<int> >(nSize, vector<int>(nSize));
         for (int i=0; i<nSize; i++) {
             for (int j=0; j<nSize; j++) {
                 int temp;
                 cin>>temp;
                 board[i][j] = temp;
//                 cout<<board[i][j]<<' ';
             }
         }
         cached = vector<vector<int> >(nSize, vector<int>(nSize, -1));
         int ans = jumpgame(0, 0);
         if (ans==1)
             cout<<"YES\n";
         else
             cout<<"NO\n";
     }
}
