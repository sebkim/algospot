
def maxPathSum(y, x):
	if y==len(triangle):
		return 0
	if cached[y][x] is not None:
		return cached[y][x]
	ret = max(triangle[y][x]+maxPathSum(y+1, x), triangle[y][x]+maxPathSum(y+1, x+1))
	cached[y][x] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	height = int(rl().strip())
	triangle = []
	for h in range(height):
		nums = [ int(i) for i in rl().strip().split(' ') ]
		triangle.append(nums)
	cached = [[None for i in range(len(triangle[-1]))] for j in range(len(triangle))]
	print maxPathSum(0, 0)	
