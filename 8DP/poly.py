
MOD = 10000000
def poly(n, first):
	if n==first:
		return 1
	if cached[n][first] is not None:
		return cached[n][first]
	ret = 0
	for second in range(1, n-first+1):
		if first==0:
			ret+=(poly(n, second) )
		else:
			ret += ( ( first+second-1 ) * poly(n-first, second) )
	ret %= MOD
	cached[n][first] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	num = int(rl().strip())
	cached = [[None for i in range(num+1)] for j in range(num+1)]
	print ( poly(num, 0) )