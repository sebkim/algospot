
def search(vil, d):
	if d==0:
		if vil==startVil:
			return 1
		else:
			return 0
	if cached[vil][d] is not None:
		return cached[vil][d]
	prob = 0
	adjs = [ iInd for iInd, i in enumerate(vilMap[vil]) if i==1]
	for eachAdj in adjs:
		prob += 1/float(vilDeg[eachAdj]) * search(eachAdj, d-1)
	cached[vil][d] = prob
	return prob

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	vilNum, targetDay, startVil = [int(i) for i in rl().strip().split(' ')]
	vilMap = []
	vilDeg = []
	for i in range(vilNum):
		oneLine = [int(i) for i in rl().strip().split(' ')]
		vilMap.append(oneLine)
		vilDeg.append(sum(oneLine))
	cached = [[None for i in range(targetDay+1)] for j in range(vilNum)]
	numTargetVil = int(rl().strip())
	targetVilProbAns = ""
	targetVils = [int(i) for i in rl().strip().split(' ')]
	for targetVilInd, targetVil in enumerate(targetVils):
		targetVilProbAns += str(search(targetVil, targetDay))
		if targetVilInd != len(targetVils)-1:
			targetVilProbAns += " "
	print targetVilProbAns

	