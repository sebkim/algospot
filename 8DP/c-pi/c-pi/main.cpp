

#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

string pis;
int cached[10000];
int INF = 987654321;

int classify(int a, int b)
{
    string subPIs = pis.substr(a, b-a+1);
    char firstChar = subPIs[0];
    bool allEqual = true;
    for (int i=1; i<subPIs.size(); i++) {
        if (subPIs[i] != firstChar) {
            allEqual = false;
            break;
        }
    }
    if (allEqual) return 1;
    bool monotone = true;
    for (int i=0; i<subPIs.size()-1; i++) {
        if ( int(subPIs[i+1]) - int(subPIs[i]) != int(subPIs[1]) - int(subPIs[0]) ) {
            monotone=false;
            break;
        }
    }
    if (monotone) {
        if(abs(int(subPIs[1]) - int(subPIs[0])) == 1) {
            return 2;
        }
    }
    bool alternating = true;
    for(int i=0;i<subPIs.size();i++) {
        if (int(subPIs[i]) != int(subPIs[i%2])) {
            alternating = false;
            break;
        }
    }
    if (alternating) return 4;
    if (monotone) return 5;
    return 10;
}

int pi(int begin)
{
    int &ret = cached[begin];
    if (begin == pis.size()) {
        return 0;
    }
    if (ret != -1) return ret;
    ret = INF;
    for (int size=3; size<6; size++) {
        if (begin+size <= pis.size()) {
            ret = min(ret, pi(begin+size) + classify(begin, begin+size-1));
        }
    }
    return ret;
}

int main() {
    ios::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>pis;
//        memset(cached, -1, sizeof(cached));
        for(int i=0;i<10000;i++) cached[i] = -1;
        printf("%d\n", pi(0));
    }
}
