
def match(patternPos, matchStrPos):
	# base case
	if patternPos == len(pattern):
		if matchStrPos == len(matchStr):
			return True
		else:
			return False
	if cached[patternPos][matchStrPos] is not None:
		return cached[patternPos][matchStrPos]
	ret = False
	if patternPos < len(pattern) and matchStrPos < len(matchStr) and\
	(pattern[patternPos] == matchStr[matchStrPos] or pattern[patternPos] == '?'):
		ret = match(patternPos + 1, matchStrPos + 1)
		cached[patternPos][matchStrPos] = ret
		return ret 
	if pattern[patternPos] == '*':
		if match(patternPos+1, matchStrPos) or (matchStrPos<len(matchStr) and match(patternPos, matchStrPos+1)):
			ret = True
	cached[patternPos][matchStrPos] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	pattern = rl().strip()
	nMatchStr = int(rl().strip())
	ans = []
	for i in range(nMatchStr):
		matchStr = rl().strip()
		cached = [[None for i in range(len(matchStr)+1)] for j in range(len(pattern)+1)]
		if match(0, 0):
			ans.append(matchStr)
	ans.sort()
	for i in ans:
		print i

# pattern = 'p*p'
# matchStr = 'pp'
# cached = [[None for i in range(len(matchStr)+1)] for j in range(len(pattern)+1)]
# print match(0, 0)