# mDays, and targetClimb are global.
def snailClimb(days, climbed, rainyProb):
	if climbed >= targetClimb:
		earlyStop = 1
		cached[days][climbed] = earlyStop
		return earlyStop
	if days == mDays:
		if climbed >= targetClimb:
			return 1
		else:
			return 0
	if cached[days][climbed] is not None:
		return cached[days][climbed]
	ret = (1-rainyProb) * snailClimb(days+1, climbed+1, rainyProb) + \
	rainyProb*snailClimb(days+1, climbed+2, rainyProb)
	cached[days][climbed] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	targetClimb, mDays = [int(i) for i in rl().strip().split(' ')]
	cached = [[None for i in range(2*mDays+1)] for j in range(mDays+1)]
	print (snailClimb(0, 0, 0.75))
	