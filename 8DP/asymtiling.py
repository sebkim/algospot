def tiling2(num):
	if num<=1:
		return 1
	if cached[num-1] is not None:
		return cached[num-1]
	ret = (tiling2(num-1) + tiling2(num-2))
	cached[num-1] = ret
	return ret
def asymtiling(num):
	if num%2==1:
		ret = ( tiling2(num) - tiling2((num-1)/2) ) % 1000000007
		return ret
	ret = ( tiling2(num) - tiling2((num-2)/2) ) % 1000000007
	ret -= tiling2( num/2 )
	ret %= 1000000007
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	num = int(rl().strip())
	cached = [None for i in range(num)]
	print asymtiling(num)