# # s is global.
# # O(n)
# def minError(a, b):
# 	m = sum(s[a:b]) / float(len(s[a:b]))
# 	m = int(round(m))
# 	ret = 0
# 	for i in range(a, b):
# 		ret += pow(s[i]-m, 2)
# 	return ret

def precalc():
	pSum = []
	pSqSum = []
	pSum.append(s[0])
	pSqSum.append(pow(s[0], 2))
	for i in range(1, lenS):
		pSum.append(pSum[i-1] + s[i])
		pSqSum.append(pSqSum[i-1] + pow(s[i], 2))
	return pSum, pSqSum

# s, pSum, pSqSum are global.
# O(1)
def minError(a, b):
	if a==0:
		hereSum = pSum[b-1]
		sqSum = pSqSum[b-1]
	else:
		hereSum = pSum[b-1] - pSum[a-1]
		sqSum = pSqSum[b-1] - pSqSum[a-1]
	m= int(round( hereSum / float( b-a ) ))
	ret = sqSum - 2*m*hereSum + m*m*(b-a)
	return ret

# s is global.
def quantize(since, parts):
	if since == lenS:
		return 0
	if parts == 0:
		return float('inf')
	if cached[since][parts-1] is not None:
		return cached[since][parts-1]
	ret = float('inf')
	for size in range(1, lenS-since+1):
		# ret = min(ret, minError(since, since+size) + quantize(since+size, parts-1))
		nextRet = minError(since, since+size) + quantize(since+size, parts-1)
		if ret > nextRet:
			ret = nextRet
			bestNext[since][parts-1] = size
	cached[since][parts-1] = ret
	return ret
def reconstruct(since, parts):
	if parts<=0:
		return
	optSize = bestNext[since][parts-1]
	print s[since:since+optSize]
	reconstruct(since+optSize, parts-1)

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	# s = [1,1,2,2,2,3,3,3,3]
	# lenS = len(s)
	# lenCand = 3

	lenS, lenCand = [ int(i) for i in rl().strip().split() ]
	s = [ int(i) for i in rl().strip().split() ]
	s.sort()
	pSum, pSqSum = precalc()
	cached = [[None for i in range(lenCand)] for j in range(lenS)]
	bestNext = [[None for i in range(lenCand)] for j in range(lenS)]
	print quantize(0, lenCand)
	print s
	reconstruct(0, lenCand)
	print

	