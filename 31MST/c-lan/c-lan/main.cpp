
#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

int nVer, nEdge;
vector<int> xs, ys;
vector<pair<int, double> > adj[500];

class UnionFind {
public:
    vector<int> parent, rank;
    UnionFind(int n) {
        for(int i=0;i<n;i++) {
            parent.push_back(i);
            rank.push_back(1);
        }
    }
    int find(int key) {
        if(parent[key] == key) return key;
        return parent[key] = find(parent[key]);
    }
    bool merge(int a, int b) {
        a=find(a); b=find(b);
        if(a==b) return false;
        if (rank[a] > rank[b]) swap(a,b);
        parent[a] = b;
        if (rank[a] == rank[b]) rank[b]+=1;
        return true;
    }
};

double kruskal(vector<pair<int, int> > &selectedEdges) {
    vector<pair<double, pair<int, int>> > edges;
    for(int here=0;here<nVer;here++) {
        for(int i=0;i<adj[here].size();i++) {
            double weight = adj[here][i].second;
            int u = here;
            int v = adj[here][i].first;
            edges.push_back(make_pair(weight, make_pair(u, v)));
        }
    }
    sort(edges.begin(), edges.end());
    double ret=0;
    UnionFind sets(nVer);
    for(int edgeInd=0;edgeInd<edges.size();edgeInd++) {
        int u=edges[edgeInd].second.first;
        int v=edges[edgeInd].second.second;
        double weight = edges[edgeInd].first;
        if(sets.find(u) == sets.find(v)) continue;
        ret += weight;
        selectedEdges.push_back(make_pair(u, v));
        sets.merge(u, v);
    }
    return ret;
}

double calcWeight(int x1, int y1, int x2, int y2) {
    return sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));
}

int main() {
    ios_base::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>nVer; cin>>nEdge;
        xs.clear(); ys.clear();
        for(int i=0;i<500;i++) adj[i].clear();
        for(int i=0;i<nVer;i++) {
            int a; cin>>a;
            xs.push_back(a);
        }
        for(int i=0;i<nVer;i++) {
            int a; cin>>a;
            ys.push_back(a);
        }
        for(int i=0;i<nEdge;i++) {
            int a,b; cin>>a; cin>>b;
            adj[a].push_back(make_pair(b, 0.0));
            adj[b].push_back(make_pair(a, 0.0));
        }
        // calculate edge weight.
        for(int i=0;i<nVer;i++) {
            for(int j=i+1;j<nVer;j++) {
                double weight = calcWeight(xs[i], ys[i], xs[j], ys[j]);
                adj[i].push_back(make_pair(j, weight));
                adj[j].push_back(make_pair(i, weight));
            }
        }
        ////
        vector<pair<int, int> > selectedEdges;
        cout.precision(10);
        cout<<kruskal(selectedEdges)<<"\n";
    }
}
