
def lis(start):
	if start==lenNums-1:
		cached[start] = 1
		return 1
	if cached[start] is not None:
		return cached[start]
	ret = 1
	for next in range(start+1, lenNums):
		if nums[start] < nums[next]:
			ret = max(ret, lis(next)+1)	
	cached[start] = ret
	return ret

def count(start):
	if lis(start) == 1: 
		countCached[start] = 1
		return 1
	if countCached[start] is not None:
		return countCached[start]
	ret = 0
	for next in range(start+1, lenNums):
		if nums[start] < nums[next] and lis(start) == lis(next)+1:
			ret += count(next)
	countCached[start] = ret
	return ret

def reconstruct(start, skip, reconList):
	if start!=-1: reconList.append(nums[start])
	# its element is (nums, idx)
	followers = []
	for next in range(start+1, lenNums):
		if (start==-1 or nums[start] < nums[next] ) and (ans==lis(next) or lis(start) == lis(next)+1):
			followers.append( (nums[next], next) )
	followers.sort()
	for eachFollower in followers:
		idx = eachFollower[1]
		cnt = count(idx)
		if cnt <= skip:
			skip -= cnt
		else:
			reconstruct(idx, skip, reconList)
			break

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenNums, k = [int(i) for i in rl().strip().split()]
	nums = [int(i) for i in rl().strip().split()]
	cached = [None for i in range(lenNums)]
	countCached = [None for i in range(lenNums)]
	ans = 0
	for eachStart in range(lenNums):
		ans = max(ans, lis(eachStart))
	# print nums, ans
	# print cached
	for i in range(lenNums):
		if countCached[i] is None:
			count(i)
	# print countCached
	reconList = []
	reconstruct(-1, k-1, reconList)
	# print reconList
	# print
	print ans
	print ' '.join([str(i) for i in reconList])
	