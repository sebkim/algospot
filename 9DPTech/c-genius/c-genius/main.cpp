

#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

int nSong, k, nFav;
vector<int> lenEachSong;
vector<vector<double> > transition;
vector<int> favSong;
double cached[4+1][50];

class SquareMatrix
{
public:
    vector<vector<double> > mat;
    SquareMatrix(int nSize) {
        mat = vector<vector<double> > (nSize, vector<double>(nSize, 0.0));
    }
    const SquareMatrix mul(const SquareMatrix &A) const {
        SquareMatrix retMat = SquareMatrix(mat.size());
        for(int i=0;i<mat.size();i++) {
            for(int j=0;j<mat.size();j++) {
                 for(int k=0;k<mat.size();k++) {
                     retMat.mat[i][j] += mat[i][k] * A.mat[k][j];
                }
            }
        }
        return retMat;
    }
};

const SquareMatrix pow(const SquareMatrix &A, int m)
{
    if(m==0) {
        SquareMatrix identity = SquareMatrix(A.mat.size());
        for(int i=0;i<A.mat.size();i++) {
            for(int j=0;j<A.mat.size();j++) {
                if (i==j) identity.mat[i][j] = 1.0;
                else identity.mat[i][j] = 0.0;
            }
        }
        return identity;
    }
    if(m%2 >0) return pow(A, m-1).mul(A);
    SquareMatrix half = pow(A, m/2);
    return half.mul(half);
}


vector<double> genius()
{
    for(int i=0;i<5;i++)
        for(int j=0;j<nSong;j++)
            cached[i][j] = 0.0;
    cached[0][0] = 1.0;
    for(int time=1;time<=k;time++) {
        for( int song=0;song<nSong;song++) {
            double &prob = cached[time%5][song];
            prob = 0;
            for(int last=0;last<nSong;last++) {
                prob += cached[ (time- lenEachSong[last] +5 )%5 ][last] * transition[last][song];
            }
        }
    }
    vector<double> ret(nFav, 0);
    for(int fav=0;fav<nFav;fav++) {
        int hereSong = favSong[fav];
        for(int start=k-lenEachSong[hereSong]+1;start<=k;start++) {
            if(start>=0)
                ret[fav] += cached[start%5][hereSong];
        }
    }
    return ret;
}

vector<double> genius2()
{
    SquareMatrix W(4*nSong);
    for(int i=0;i<3*nSong;i++)
        W.mat[i][i+nSong] = 1.0;
    for(int i=0;i<nSong;i++)
        for(int j=0;j<nSong;j++)
            W.mat[3*nSong+i][(4-lenEachSong[j])*nSong+j] = transition[j][i];
    SquareMatrix Wk = pow(W, k);
    vector<double> ret(nFav);
    for(int fav=0; fav<nFav;fav++) {
        int hereSong = favSong[fav];
        for(int start=0;start<lenEachSong[hereSong];start++) {
            ret[fav] += Wk.mat[(3-start)*nSong + hereSong][3*nSong];
        }
    }
    return ret;
}

int main() {
    ios::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>nSong; cin>>k; cin>>nFav;
        lenEachSong = vector<int>(nSong);
        transition = vector<vector<double>> (nSong, vector<double>(nSong));
        favSong = vector<int>(nFav);
        for(int i=0;i<nSong;i++) cin>>lenEachSong[i];
        for(int i=0;i<nSong;i++)
            for(int j=0;j<nSong;j++)
                cin>>transition[i][j];
        for(int i=0;i<nFav;i++) cin>>favSong[i];
        vector<double> ans = genius2();
        cout.precision(17);
        for(int i=0;i<ans.size();i++) {
            cout<<fixed<<ans[i];
//            printf("%lf", ans[i]);
            if (i!=ans.size()-1) cout<<" ";
        }
        cout<<"\n";
    }
}
