from copy import deepcopy
from pprint import pprint
BRICKTYPE = [
[[0, 0], [1, 0], [0, 1]],
[[0, 0], [0, 1], [1, 1]],
[[0, 0], [1, 0], [1, 1]],
[[0, 0], [1, 0], [1, -1]],
[[0, 0], [1, 0]],
[[0, 0], [0, 1]]
]

# delta +1 means putBrick, and delta -1 means offBrick
# if there is no problem in putting brick, return true. Else, false.
def PutBrick(board, y, x, b_type, delta):
	ret = True
	brick = BRICKTYPE[b_type]
	new_board = deepcopy(board)
	for i in range(len(brick)):
		ny = y + brick[i][0]
		nx = x + brick[i][1]
		if ny >= size or nx>= size or ny<0 or nx<0:
			ret = False
		else:
			new_board[ny][nx] += delta
			if new_board[ny][nx] >= 2:
				ret=False
	if ret == True:
		return new_board
	return ret

def blockgame(board):
	fs = frozenset()
	for raw_ind, each_line in enumerate(board):
		for each_ind, each in enumerate(each_line):
			if each==1:
				fs |= frozenset(((raw_ind, each_ind), ))
	if cached.get(fs) is not None: return cached.get(fs)
	ret = False
	for empty_y in xrange(size):
		for empty_x in xrange(size):
			if board[empty_y][empty_x] == 0:
				for each_brick in xrange(6):
					new_board = PutBrick(board, empty_y, empty_x, each_brick, 1)
					if new_board:
						# new fs update
						brick = BRICKTYPE[each_brick]
						for i in range(len(brick)):
							ny = empty_y + brick[i][0]
							nx = empty_x + brick[i][1]
							new_fs = fs | frozenset(((ny, nx), ))
						###
						if (blockgame(new_board) == False):
							ret = True
							cached[new_fs] = True
							return ret
	cached[fs] = ret
	return ret

size=5
import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	board = []
	for i in range(size):
		oneLine = rl().strip().split()[0]
		oneLine = [0 if i=='.' else 1 for i in oneLine]
		board.append(oneLine)
	cached = {}
# 	pprint(board)
	ans = blockgame(board)
	if ans==True:
		print 'WINNING'
	else:
		print 'LOSING'
