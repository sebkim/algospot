
def packing(item, capacity):
	if item == nItem: return 0
	if cached[item][capacity] is not None:
		return cached[item][capacity]
	ret = packing(item+1, capacity)
	if capacity >= capas[item]:
		ret = max(ret, packing(item+1, capacity-capas[item]) + needScore[item])
	cached[item][capacity] = ret
	return ret

def reconstruct(item, capacity, reconList):
	if item == nItem: return
	if packing(item, capacity) == packing(item+1, capacity):
		reconstruct(item+1, capacity, reconList)
	else:
		reconList.append(itemNames[item])
		reconstruct(item+1, capacity - capas[item], reconList)

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nItem, maxCapa = [int(i) for i in rl().strip().split()]
	itemNames = []
	capas = []
	needScore = []
	for i in range(nItem):
		oneLine = [int(j) if jInd>0 else j for jInd, j in enumerate(rl().strip().split())]
		itemNames.append(oneLine[0])
		capas.append(oneLine[1])
		needScore.append(oneLine[2])
	cached = [[None for i in range(maxCapa+1)] for j in range(nItem)]
	reconList = []
	ans = packing(0, maxCapa)
	reconstruct(0, maxCapa, reconList)
	print ans, len(reconList)
	for eachName in reconList:
		print eachName
