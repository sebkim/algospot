

def numbergame(left, right):
	if left>right: return 0
	if cached[left][right] is not None:
		return cached[left][right]
	# select one number on either sides
	# ret = max(nums[left] - numbergame(left+1, right), nums[right] - numbergame(left, right-1))
	leftCase = nums[left] - numbergame(left+1, right)
	rightCase = nums[right] - numbergame(left, right-1)
	if leftCase < rightCase:
		ret = rightCase
		bestNext[left][right] = 'right'
	else:
		ret = leftCase
		bestNext[left][right] = 'left'
	# discard two numbers on either sides
	if right-left+1 >= 2:
		# ret = max(ret, -numbergame(left+2, right))
		# ret = max(ret, -numbergame(left, right-2))
		leftRemoveCase = -numbergame(left+2, right)
		rightRemoveCase = - numbergame(left, right-2)
		if ret < leftRemoveCase:
			ret = leftRemoveCase
			bestNext[left][right] = 'leftRemove'
		if ret < rightRemoveCase:
			ret = rightRemoveCase
			bestNext[left][right] = 'rightRemove'
	cached[left][right] = ret
	return ret

def reconstruct(left, right, reconList):
	if left>right:
		print reconList
		return
	next = bestNext[left][right]
	if next=='left':
		reconList.append(('left', nums[left]))
		reconstruct(left+1, right, reconList)
	elif next=='right':
		reconList.append(('right', nums[right]))
		reconstruct(left, right-1, reconList)
	elif next=='leftRemove':
		reconList.append('leftRemove')
		reconstruct(left+2, right, reconList)
	elif next=='rightRemove':
		reconList.append('rightRemove')
		reconstruct(left, right-2, reconList)

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenNums = int(rl().strip())
	nums = [int(i) for i in rl().strip().split()]
	cached = [[None for i in range(lenNums)] for j in range(lenNums)]
	bestNext = [[None for i in range(lenNums)] for j in range(lenNums)]
	print nums
	print numbergame(0, lenNums-1)
	reconstruct(0, lenNums-1, [])
	print