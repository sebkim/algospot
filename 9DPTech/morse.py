
def calcBino(t):
	for i in range(1, t+1):
		binoList[i][0] = 1; binoList[i][i] = 1;
		for j in range(1, i):
			binoList[i][j] = binoList[i-1][j-1] + binoList[i-1][j]

def generate(n, m, retStr):
	global skip
	if skip < 0:
		return
	if n==0 and m==0:
		if skip==0: 
			print retStr
		skip-=1
		return
	if binoList[n+m][n] <= skip:
		skip -= binoList[n+m][n]
		return
	if n>0: generate(n-1, m, retStr + '-')
	if m>0: generate(n, m-1, retStr + 'o')

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	n, m, k = [int(i) for i in rl().strip().split()]
	binoList  = [[None for i in range(n+m+1)] for j in range(n+m+1)]
	calcBino(n+m)
	skip = k-1
	generate(n, m, '')
