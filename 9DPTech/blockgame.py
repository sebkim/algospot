
def cell(y, x):
	return (1<<(size*y+x))
def precalc():
	# store kieok shapes
	for y in range(size-1):
		for x in range(size-1):
			cells= []
			for dy in range(2):
				for dx in range(2):
					cells.append(cell(y+dy, x+dx))
			square = sum(cells)
			for i in range(4):
				moves.append(square - cells[i])
	# store two block line shapes
	for y in range(size):
		for x in range(size-1):
			moves.append(cell(y, x) + cell(y, x+1))
			moves.append(cell(x, y) + cell(x+1, y))

def blockgame(board):
	if cached[board] != -1:
		return cached[board]
	ret = 0
	for eachMove in moves:
		if (board & eachMove) == 0:
			if(not blockgame(board | eachMove)):
				ret = 1
				bestNext[board] = eachMove
				break
			else:
				bestNext[board] = eachMove
	cached[board] = ret
	return ret

def decompose(oneMove):
	ret = []
	binStr = bin(oneMove)[2:]
	for eachBinInd, eachBin in enumerate(reversed(binStr)):
		if eachBin == '1':
			yInd = eachBinInd / size
			xInd = eachBinInd % size
			ret.append((yInd, xInd))
	return ret

def reconstruct(board, reconList):
	next = bestNext[board]
	if next == -1:
		print reconList
		return
	reconList.append(decompose(next))
	reconstruct(board | next, reconList)

size = 5
moves = []
precalc()

import sys
from array import array
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	board = []
	for i in range(size):
		oneLine = rl().strip().split()[0]
		board.append(oneLine)
	# board preprocess
	boardVal = 0
	for y in range(size):
		for x in range(size):
			if board[y][x] == '#': boardVal += cell(y, x)
	##
	# cached = array('h', [-1 for i in xrange((1<<size*size))])
	cached = [-1 for i in range((1<<size*size))]
	bestNext = [-1 for i in range((1<<size*size))]
	for i in board:
		print i
	ans = blockgame(boardVal)
	if ans==1:
		print 'WINNING'
	else:
		print 'LOSING'
	reconstruct(boardVal, [])
	print
	
