
def sushi():
	cached[0] = 0
	for budget in range(1, money+1):
		cached[budget%201] = 0
		for eachItem in range(nItem):
			if budget >= moneyNeeded[eachItem]:
				cached[budget%201] = max(cached[budget%201], score[eachItem] + cached[(budget - moneyNeeded[eachItem]) % 201])
	return cached[money%201]

# combination of greedy and dynamic
def sushi2():
	cached[0] = 0
	if money >= pow(10, 4):
		moneyMiddle = money * 19// 20
	else:
		moneyMiddle = money * 5 // 100
	for budget in range(1, moneyMiddle):
		cached[budget%201] = 0
		if budget >= moneyNeeded[bestEffiItem[0]]:
			cached[budget%201] = score[bestEffiItem[0]] + cached[ (budget - moneyNeeded[bestEffiItem[0]]) % 201 ]
	for budget in range(moneyMiddle, money+1):
		cached[budget%201] = 0
		for eachItem in range(nItem):
			if budget >= moneyNeeded[eachItem]:
				cached[budget%201] = max(cached[budget%201], score[eachItem] + cached[(budget - moneyNeeded[eachItem]) % 201])
	return cached[money%201]

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nItem, money = [int(i) for i in rl().strip().split()]
	moneyNeeded = []
	score = []
	for i in range(nItem):
		oneLine = [int(i) for i in rl().strip().split()]
		moneyNeeded.append(oneLine[0]//100)
		score.append(oneLine[1])
	money = money // 100
	cached = [None for i in range(201)]
	# print (sushi())
	# print cached
	effi = [s / float(m) for m, s in zip(moneyNeeded, score)]
	bestEffiItem = max(enumerate(effi), key=lambda x:x[1])
	print (sushi2())