

def overlap(a, b):
	ans = 0
	for aRevInd, bInd in zip(range(len(a)-1, -1, -1), range(1, len(b)+1)):
		if bInd > len(b) or aRevInd <0:
			break
		if a[aRevInd:] == b[:bInd]:
			ans = max(ans, len(a[aRevInd:]))
	return ans

def restore(last, used):
	if used == ((1<<len(subStrs))-1):
		return 0
	if cached[last][used] is not None:
		return cached[last][used]
	ret = 0
	for next in range(len(subStrs)):
		if used & (1<<next) == 0:
			cand = preOverlap[last][next] + restore(next, used | (1<<next))
			if cand >= ret:
				ret = cand
				bestNext[last][used] =next
	cached[last][used] = ret
	return ret

def reconstruct(last, used):
	if used == ((1<<len(subStrs))-1):
		return ""
	next = bestNext[last][used]
	return subStrs[next][preOverlap[last][next]:] + reconstruct(next, used | (1<<next))

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nSub = int(rl().strip())
	subStrs = []
	for eachSubInd in range(nSub):
		subStrs.append(rl().strip())
	preproSubStrs = []
	for eachSub in subStrs:
		dupBool = False
		for eachSub2 in subStrs:
			if eachSub != eachSub2:
				if eachSub in eachSub2:
					dupBool = True
					break
				else:
					pass
		if dupBool == False:
			preproSubStrs.append(eachSub)
	subStrs = preproSubStrs
	preOverlap = [[0 for i in range(len(subStrs))] for j in range(len(subStrs))]
	for eachSubInd, eachSub in enumerate(subStrs):
		for eachSubInd2, eachSub2 in enumerate(subStrs):
			if eachSubInd != eachSubInd2:
				preOverlap[eachSubInd][eachSubInd2] = overlap(eachSub, eachSub2)
	cached = [[None for i in range(1<<len(subStrs))] for j in range(len(subStrs))]
	bestNext = [[None for i in range(1<<len(subStrs))] for j in range(len(subStrs))]
	ans = 0
	for i in range(len(subStrs)):
		cand = restore(i, 1<<i)
		if ans <= cand:
			ans = cand
			startingPos = i
	print subStrs[startingPos]+reconstruct(startingPos, 1<<startingPos)
