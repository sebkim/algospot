import copy
# edible is a list that has the number of foods that this person can eat
def allergy(edible, chosen):
	global best
	if best <= chosen:
		return
	# choose first person that cannot eat any foods
	firstPer = 0
	while firstPer < nPer and edible[firstPer] != 0:
		firstPer+=1
	if firstPer == nPer:
		if best > chosen:
			best = chosen
		return
	for eachFood in canEat[firstPer]:
		for thisFoodForWho in eaters[eachFood]:
			edible[thisFoodForWho]+=1
		allergy(edible, chosen+1)
		for thisFoodForWho in eaters[eachFood]:
			edible[thisFoodForWho]-=1
# '2' version optimize further.
# it choose the person who can eat less number of foods.
# and also choose foods first that can be eaten by more number of people.
def allergy3(edible, chosen):
	global best
	if best <= chosen:
		return
	personThatHasLessFoodList = [_[0] for _ in sorted(enumerate(edible), key=lambda x:x[1]) if _[1]==0]
	if len(personThatHasLessFoodList) == 0:
		if best > chosen: best = chosen
		return
	modiCanEat = {}
	for eachPer in personThatHasLessFoodList:
		thisPersonFood = []
		for eachFood in canEat[eachPer]:
			if people[eachPer] in foods[eachFood]:
				thisPersonFood.append(eachFood)
		modiCanEat[eachPer] = thisPersonFood
	personThatHasLessFood = sorted( [ (k, len(v)) for k,v in modiCanEat.items() ], key=lambda x:x[1])[0][0]
	
	# chooses food first that can be eaten by more number of people.
	modiEaters = {}
	for eachFood in canEat[personThatHasLessFood]:
		oneLine = []
		for eachPer in personThatHasLessFoodList:
			if people[eachPer] in foods[eachFood]:
				oneLine.append(eachPer)
		modiEaters[eachFood] = oneLine
	sortedCanEat = sorted( [ (k, len(v)) for k,v in modiEaters.items() ], key=lambda x:x[1], reverse=True)

	for eachFood, numPeopleCanEatThis in sortedCanEat:
		for thisFoodForWho in eaters[eachFood]:
			edible[thisFoodForWho]+=1
		allergy3(edible, chosen+1)
		for thisFoodForWho in eaters[eachFood]:
			edible[thisFoodForWho]-=1

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nPer, nFood = [int(i) for i in rl().strip().split()]
	people = [i for i in rl().strip().split()]
	foods = []
	for i in range(nFood):
		oneLine = [i for iInd, i in enumerate(rl().strip().split()) ]
		foods.append(oneLine)
	eaters = []
	for eachFood in foods:
		oneLine = []
		for eachPerInd, eachPer in enumerate(people):
			if eachPer in eachFood:
				oneLine.append(eachPerInd)
		eaters.append(oneLine)
	best = float('inf')
	edible = [0 for i in range(nPer)]
	canEat = []
	for eachPer in range(nPer):
		thisPersonFood = []
		for eachFood in range(nFood):
			if people[eachPer] in foods[eachFood]:
				thisPersonFood.append(eachFood)
		canEat.append(thisPersonFood)
	# allergy(edible, 0)
	allergy3(edible, 0)
	print best
