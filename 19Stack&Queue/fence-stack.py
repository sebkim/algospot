
def fence(h):
	stack = []
	leftWallInd = [ None for i in range(len(h)) ]
	leftWallInd[0] = -1
	stack.append(0)
	maxH = 0
	for i in range(1, len(h)):
		if h[stack[-1]] < h[i]:
			leftWallInd[i] = stack[-1]
			stack.append(i)
		elif h[stack[-1]] >=h[i]:
			popped = stack.pop()
			rightWallInd = i
			maxH = max(maxH, (rightWallInd - leftWallInd[popped] - 1) * h[popped])
			while len(stack)>0 and h[stack[-1]] >= h[i]:
				popped = stack.pop()
				maxH = max(maxH, (rightWallInd - leftWallInd[popped] -1) * h[popped])
			leftWallInd[i] = leftWallInd[popped]
			stack.append(i)
	# process remaining elements in stack
	rightWallInd = len(h)
	while len(stack)>0:
		popped = stack.pop()
		maxH = max(maxH, (rightWallInd - leftWallInd[popped] -1) * h[popped])
	return maxH

import sys
from array import array

rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenH = int(rl().strip())
	# h = [ int(i) for i in rl().strip().split(' ') ]
	h = array('H', [ int(i) for i in rl().strip().split(' ') ])
	print fence(h)
