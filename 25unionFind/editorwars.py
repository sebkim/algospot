
class biUnionFind(object):
	def __init__(self, n):
		self.parent = range(n)
		self.rank = [1 for i in range(n)]
		self.enemy = [-1 for i in range(n)]
		self.size = [1 for i in range(n)]
	def find(self, key):
		if key==self.parent[key]: return key
		self.parent[key] = self.find(self.parent[key])
		return self.parent[key]
	def merge(self, a, b):
		if a==-1 or b==-1: return max(a, b)
		a = self.find(a); b = self.find(b);
		if a==b: return a
		if self.rank[a] > self.rank[b]:
			a, b = b, a
		self.parent[a] = b
		if self.rank[a] == self.rank[b]:
			self.rank[b]+=1
		self.size[b] += self.size[a]
		return b
	def ack(self, a, b):
		a=self.find(a); b=self.find(b);
		if self.enemy[a]==b: return False
		u = self.merge(a, b)
		v = self.merge(self.enemy[a], self.enemy[b])
		self.enemy[u] = v
		if v!=-1: self.enemy[v] = u
		return True
	def dis(self, a, b):
		a=self.find(a); b=self.find(b);
		if a==b: return False
		u = self.merge(a, self.enemy[b])
		v = self.merge(self.enemy[a], b)
		self.enemy[u] = v
		self.enemy[v] = u
		return True

def createBUF(nPer, commData):
	buf = biUnionFind(nPer)
	for eachCommInd, eachComm in enumerate(commData):
		if eachComm[0] == 'ACK':
			ans = buf.ack(eachComm[1], eachComm[2])
		elif eachComm[0	] == 'DIS':
			ans = buf.dis(eachComm[1], eachComm[2])
		if ans is False:
			return (False, eachCommInd+1)
	return (True, buf)

def solve(buf):
	ret = 0
	for eachPer in range(nPer):
		# if it is root node
		if eachPer == buf.find(eachPer):
			enemy = buf.enemy[eachPer]
			# not to count duplicates
			if enemy > eachPer: continue
			mySize = buf.size[eachPer]
			enemySize = buf.size[buf.find(enemy)] if enemy!=-1 else 0
			ans = max(mySize, enemySize)
			ret += ans
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
# n=1
for eachCase in range(n):
	nPer, nComm = [int(i) for i in rl().strip().split()]
	commData = []
	for j in range(nComm):
		oneLine = [int(i) if iInd>0 else i for iInd, i in enumerate(rl().strip().split())]
		commData.append(oneLine)
	bufWithBool = createBUF(nPer, commData)
	buf = bufWithBool[1]
	if bufWithBool[0] is False:
		print "CONTRADICTION AT {}".format(buf)
	else:
		print "MAX PARTY SIZE IS {}".format(solve(buf))
		# print buf.parent, buf.enemy, buf.size
