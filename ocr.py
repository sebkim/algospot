
def recognize(segment, previousMatch):
	if segment == lenSeg:
		return 0
	if cached[segment][previousMatch] is not None:
		return cached[segment][previousMatch]
	ret = -float('inf')
	for thisMatch in range(lenWord):
		cand = transitionMat[previousMatch][thisMatch] + emissionMat[thisMatch][recognizedSeg[segment]] + \
		recognize(segment+1, thisMatch+1)
		if ret < cand:
			ret = cand
			chosen[segment][previousMatch] = thisMatch
	cached[segment][previousMatch] = ret
	return ret

def reconstruct_original(segment, previousMatch):
	whichChosen = chosen[segment][previousMatch]
	ret = words[whichChosen]
	if segment < lenSeg-1:
		ret += ' ' + reconstruct_original(segment+1, whichChosen+1)
	return ret

import sys
import math
rl = lambda: sys.stdin.readline()

lenWord, testSize = [int(i) for i in rl().strip().split()]
words = [i for i in rl().strip().split()]
transitionMat = []
for i in range(lenWord+1):
	oneRow = [math.log(float(i2)) if float(i2)!=0.0 else -float('inf') for i2 in rl().strip().split()]
	transitionMat.append(oneRow)
emissionMat = []
for i in range(lenWord):
	oneRow = [math.log(float(i2)) if float(i2)!=0.0 else -float('inf') for i2 in rl().strip().split()]
	emissionMat.append(oneRow)

for t in range(testSize):
	testCase = [i for i in rl().strip().split()]
	lenSeg = int(testCase[0])
	recognizedSeg = [words.index(i) for i in testCase[1:]]
	cached = [[None for i in range(lenWord+1)] for j in range(lenSeg)]
	chosen = [[None for i in range(lenWord+1)] for j in range(lenSeg)]
	recognize(0, 0)
	print reconstruct_original(0, 0)

