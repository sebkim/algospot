import pyximportcpp; pyximportcpp.install();
from treap import *
import sys
from random import shuffle

upto = int(sys.argv[1])
testData = range(1, upto)

# test(testData)
testSet(testData)

t = treap()
shuffle(testData)
for i in testData:
	t.insertNode(i)
print "Height is {}, and size is {}.".format(t.rootHeight(), t.getSize())
# shuffle(testData)
# for i in testData:
# 	t.eraseNode(i)
# print "Height is {}, and size is {}.".format(t.rootHeight(), t.getSize())
