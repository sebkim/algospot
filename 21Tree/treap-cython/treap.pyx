#cython: profile = True

from random import shuffle
cimport cython
from libc.stdlib cimport malloc, free
from cython.operator cimport dereference as deref
from random import randint
from libcpp.pair cimport pair
from libcpp.string cimport string
from libcpp cimport bool

from libcpp.set cimport set as cset


cdef inline int inlMax(int a, int b):
	if a>b: return a
	else: return b

cdef struct treapNode:
	int size
	int priority
	treapNode *left
	treapNode *right
	treapNode *parent
	int key
ctypedef treapNode *tnPointer

cdef class treap:
	cdef:
		treapNode *root;
	def __init__(self):
		self.root = NULL
	@staticmethod
	cdef viewTree(treapNode *node, level=0):
		ret = "\t"*level+repr(node.key)+', '+repr(node.priority)
		if node.parent:
			ret += ', '+ repr(node.parent.key)
		ret += '\n'
		if node.left:
			ret += treap.viewTree(node.left, level=level+1)
		else:
			ret += "\t"*(level+1) + 'Left None.\n'
		if node.right:
			ret += treap.viewTree(node.right, level=level+1)
		else:
			ret += "\t"*(level+1) + 'Right None.\n'
		return ret
	@staticmethod
	cdef treapNode *initNode(int key):
		cdef treapNode *tn = <treapNode*>malloc(1*sizeof(treapNode))
		tn[0].size=1
		tn[0].left=NULL
		tn[0].right=NULL
		tn[0].parent = NULL
		tn[0].key = key
		tn[0].priority = randint(0, 987654321)
		return tn
	@staticmethod
	cdef void calcSize(treapNode *node):
		node[0].size=1
		if node[0].left: node[0].size += node[0].left[0].size
		if node[0].right: node[0].size += node[0].right[0].size
	@staticmethod
	cdef void setLeft(treapNode *node, treapNode *newLeft):
		node[0].left = newLeft
		treap.calcSize(node)
	@staticmethod
	cdef void setRight(treapNode *node, treapNode *newRight):
		node[0].right = newRight
		treap.calcSize(node)
	cpdef int rootHeight(self):
		if self.root != NULL:
			return treap.height(self.root)
		return 0
	@staticmethod
	cdef int height(treapNode *node):
		cdef:
			int leftHeight
			int rightHeight
		if node.left:
			leftHeight = treap.height(node.left)
		else:
			leftHeight=0
		if node.right:
			rightHeight = treap.height(node.right)
		else:
			rightHeight=0
		return inlMax(leftHeight, rightHeight)+1
	# return NodePair.
	# left one is less than key, and right one is equal or greater than key.
	@staticmethod
	cdef pair[tnPointer, tnPointer] split(treapNode *node, int key):
		cdef pair[tnPointer, tnPointer] ret;
		if node == NULL:
			ret.first = NULL
			ret.second = NULL
			return ret
		cdef pair[tnPointer, tnPointer] nodepair;
		if node[0].key<key:
			nodepair= treap.split(node[0].right, key)
			treap.setRight(node, nodepair.first)
			if nodepair.first != NULL:
				nodepair.first[0].parent = node
			ret.first = node
			ret.second = nodepair.second
			return ret
		nodepair = treap.split(node[0].left, key)
		treap.setLeft(node, nodepair.second)
		if nodepair.second != NULL:
			nodepair.second[0].parent = node
		ret.first = nodepair.first
		ret.second = node
		return ret
	# return root node after inserting
	@staticmethod
	cdef treapNode *insert(treapNode *node, treapNode *newNode, treapNode *parentParam = NULL):
		if node == NULL:
			newNode[0].parent = parentParam
			return newNode
		cdef pair[tnPointer, tnPointer] nodepair;
		if node[0].priority < newNode[0].priority:
			nodepair = treap.split(node, newNode[0].key)
			treap.setLeft(newNode, nodepair.first)
			if nodepair.first != NULL:
				nodepair.first[0].parent = newNode
			treap.setRight(newNode, nodepair.second)
			if nodepair.second != NULL:
				nodepair.second[0].parent = newNode
			newNode[0].parent = parentParam
			return newNode
		elif newNode[0].key < node[0].key:
			treap.setLeft(node, treap.insert(node.left, newNode, node))
		else:
			treap.setRight(node, treap.insert(node.right, newNode, node))
		return node
	cpdef void insertNode(self, int key):
		cdef treapNode *newNode = treap.initNode(key)
		self.root = treap.insert(self.root, newNode)
	# max(root.key) < min(mergedNode.key)
	@staticmethod
	cdef treapNode *merge(treapNode *node, treapNode *mergedNode):
		if node == NULL: return mergedNode
		if mergedNode == NULL: return node
		cdef treapNode *mer;
		if node[0].priority < mergedNode[0].priority:
			mer = treap.merge(node, mergedNode.left)
			treap.setLeft(mergedNode, mer)
			if mer != NULL:
				mer[0].parent = mergedNode
			return mergedNode
		mer = treap.merge(node.right, mergedNode)
		treap.setRight(node, mer)
		if mer !=NULL:
			mer[0].parent = node
		return node
	# return root node after erasing
	@staticmethod
	cdef treapNode *erase(treapNode *node, int key):
		if node == NULL: return NULL
		cdef treapNode *retNode
		if node[0].key == key:
			retNode = treap.merge(node.left, node.right)
			if retNode != NULL:
				retNode[0].parent = node[0].parent
			free(node)
			return retNode
		cdef treapNode *rootAfterErase;
		if key < node[0].key:
			rootAfterErase = treap.erase(node.left, key)
			treap.setLeft(node, rootAfterErase)
		else:
			rootAfterErase = treap.erase(node.right, key)
			treap.setRight(node, rootAfterErase)
		return node
	# it return True if successfully erased
	cpdef bool eraseNode(self, int key):
		if self.root == NULL:
			return False	
		cdef treapNode *afterErase = treap.erase(self.root, key)
		cdef bool ret = True
		if afterErase != NULL and afterErase[0].size == self.root[0].size:
			ret = False
		self.root = afterErase
		return ret
	cpdef int getSize(self):
		if self.root != NULL:
			return self.root[0].size
		return 0

def test(testData):
	t = treap()
	shuffle(testData)
	cdef int i
	for i in testData:
		t.insertNode(i)
		print 'Insert {}'.format(i)
		print "Height is {}, and size is {}.".format(t.rootHeight(), t.getSize())
		print treap.viewTree(t.root)
		print
	# print "Height is {}, and size is {}.".format(t.rootHeight(), t.getSize())
	# print treap.viewTree(t.root)

def testSet(list testData):
	cdef cset[int] s;
	cdef int i;
	shuffle(testData)
	for i in testData:
		s.insert(i)
	shuffle(testData)
	for i in testData:
		s.erase(i)
