class treap(object):
    def __init__(self, key=None, value=None):
        import random
        import sys
        self.key = key
        self.value =value
        self.size = 1
        self.priority = random.randint(0, sys.maxint)
        self.left = None
        self.right = None
        self.parent = None
    def setLeft(self, newLeft):
        self.left = newLeft
        self.calcSize()
    def setRight(self, newRight):
        self.right =newRight
        self.calcSize()
    def calcSize(self):
        self.size=1
        if(self.left): self.size += self.left.size
        if(self.right): self.size += self.right.size
    # return NodePair.
    # left one is less than key, and right one is equal or greater than key.
    def split(self, key):
        if self.key < key:
            if self.right:
                nodepair = self.right.split(key)
            else:
                nodepair = (None, None)
            self.setRight(nodepair[0])
            if nodepair[0] is not None:
                nodepair[0].parent = self
            return (self, nodepair[1])
        if self.left:
            nodepair = self.left.split(key)
        else:
            nodepair = (None, None)
        self.setLeft(nodepair[1])
        if nodepair[1] is not None:
            nodepair[1].parent = self
        return (nodepair[0], self)
    # return root node after inserting
    def insert(self, newNode, parentParam = None):
        if self.priority < newNode.priority:
            nodepair = self.split(newNode.key)
            newNode.setLeft(nodepair[0])
            if nodepair[0] is not None:
                nodepair[0].parent = newNode
            newNode.setRight(nodepair[1])
            if nodepair[1] is not None:
                nodepair[1].parent = newNode
            newNode.parent = parentParam
            return newNode
        elif self.key > newNode.key:
            if self.left is not None:
                self.setLeft(self.left.insert(newNode, self))
            else:
                self.setLeft(newNode)
                newNode.parent = self
        else:
            if self.right is not None:
                self.setRight(self.right.insert(newNode, self))
            else:
                self.setRight(newNode)
                newNode.parent = self
        return self
    def height(self):
        if self.left:
            leftHeight = self.left.height()
        else:
            leftHeight = 0
        if self.right:
            rightHeight = self.right.height()
        else:
            rightHeight = 0
        return max(leftHeight, rightHeight) +1
    def merge(self, mergedNode):
        if self.priority < mergedNode.priority:
            if mergedNode.left:
                mer = self.merge(mergedNode.left)
            else:
                mer = self
            mergedNode.setLeft(mer)
            if mer:
                mer.parent = mergedNode
            return mergedNode
        if self.right:
            mer = self.right.merge(mergedNode)
        else:
            mer = mergedNode
        self.setRight(mer)
        if mer:
            mer.parent = self
        return self
    # return root node after erasing
    def erase(self, key):
        if self.key == key:
            if self.left and not self.right:
                retNode = self.left
            elif not self.left and self.right:
                retNode = self.right
            elif not self.left and not self.right:
                retNode = None
            else:
                retNode = self.left.merge(self.right)
            if retNode:
                retNode.parent = self.parent
            del self
            return retNode
        if key < self.key:
            if self.left:
                erasedRoot = self.left.erase(key)
                self.setLeft(erasedRoot)
            else:
                pass
        else:
            if self.right:
                erasedRoot = self.right.erase(key)
                self.setRight(erasedRoot)
            else:
                pass
        return self

    def __str__(self, level=0):
        ret = "\t"*level+repr(self.key)+', '+repr(self.priority)
        if self.parent:
            ret += ', '+ repr(self.parent.key)
        ret += '\n'
        if self.left:
            ret += self.left.__str__(level=level+1)
        else:
            ret += "\t"*(level+1) + 'Left None.\n'
        if self.right:
            ret += self.right.__str__(level=level+1)
        else:
            ret += "\t"*(level+1) + 'Right None.\n'
        return ret
    # def verifyParent(self):
    #   if self.left:
    #       self.left.helper()
    #   self.helper()
    #   if self.right:
    #       self.right.helper()
    # def helper(self):
    #   if self.left:
    #       if self != self.left.parent:
    #           print 'oh'
    #   if self.right:
    #       if self != self.right.parent:
    #           print 'oh2'
    def search(self, x):
        if x < self.key:
            if self.left is None:
                return None
            return self.left.search(x)
        elif x > self.key:
            if self.right is None:
                return None
            return self.right.search(x)
        else:
            return self
    def successor(self):
        if self.right is not None:
            succ = self.right
            while succ.left:
                succ = succ.left
            return succ
        # search up direction
        else:
            curNode = self
            if curNode.parent is None:
                return None
            while curNode.parent is not None and curNode.parent.right == curNode:
                curNode = curNode.parent
            if curNode.parent is not None and curNode.parent.left == curNode:
                return curNode.parent
            else:
                return None
    def predecessor(self):
        if self.left is not None:
            succ = self.left
            while succ.right:
                succ = succ.right
            return succ
        # search up direction
        else:
            curNode = self
            if curNode.parent is None:
                return None
            while curNode.parent is not None and curNode.parent.left == curNode:
                curNode = curNode.parent
            if curNode.parent is not None and curNode.parent.right == curNode:
                return curNode.parent
            else:
                return None
    def kth(self, k):
        leftSize = 0
        if self.left:
            leftSize = self.left.size
        if k<= leftSize:
            return self.left.kth(k)
        if k==leftSize+1:
            return self.key
        return self.right.kth(k-leftSize-1)
def lower_bound(node, key, lb = None):
    if node:
        if node.key >= key:
            return lower_bound(node.left, key, node)
        else:
            return lower_bound(node.right, key, lb)
    return lb
def upper_bound(node, key, ub = None):
    if node:
        if node.key > key:
            return upper_bound(node.left, key, node)
        else:
            return upper_bound(node.right, key, ub)
    return ub

def isDominated(x, y):
    if tree is None:
        return False
    node = lower_bound(tree, x)
    ret = False
    if node is not None:
        if y < node.value:
            ret = True
    return ret

def removeAllDominated(x, y):
    global tree
    node = tree.search(x)
    pred = node.predecessor()
    while pred is not None:
        if y > pred.value:
            tree = tree.erase(pred.key)
            # node = tree.search(x)
            pred = node.predecessor()
        else:
            break

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
# n = 50
for eachCase in range(n):
    perNum = int(rl().strip())
    data = []
    for i in range(perNum):
        numProb, numRamen = [int(j) for j in rl().strip().split()]
        data.append([numProb, numRamen])
    
    # data = [ [1, 100], [100, 1], [50, 50], [45, 55], [55, 70] ]
    # data = [ [1,5], [2,4], [3,3], [4,2], [5,1] ]
    # xs = list(range(50000))
    # ys = list(range(50000))
    # import random
    # random.shuffle(xs)
    # random.shuffle(ys)
    # data = []
    # for x, y in zip(xs, ys):
    #     data.append([x, y])

    ans = 0
    tree = None
    for eachData in data:
        if not isDominated(eachData[0], eachData[1]):
            if tree is None:
                tree = treap(eachData[0], eachData[1])
            else:
                tree = tree.insert(treap(eachData[0], eachData[1]))
            removeAllDominated(eachData[0], eachData[1])
            ans += tree.size
            # ans += len(tree)
    print(ans)
