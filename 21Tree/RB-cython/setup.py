from distutils.core import setup
from Cython.Build import cythonize

setup(name="rbTree",
      ext_modules=cythonize("rbTree.pyx"))

