# distutils: language=c++
# cython: profile = True

from libc.stdio cimport printf
from cython.operator cimport dereference as deref

cdef extern from "RedBlackTree.h":
    cdef cppclass RedBlackTree[T1, T2]:
        cppclass rbNode:
            T1 key
            T2 value
            rbNode *left, *right, *parent
            int color
        rbNode *root
        void insert(T1, T2)
        void printTree()
        int depth()
        void clear()
        void clear(rbNode *)
        rbNode *search(T1)

cdef class cRBTree:
    cdef RedBlackTree[int, int] *_thisptr
    def __cinit__(self):
        self._thisptr = new RedBlackTree[int, int]()
    def __dealloc__(self):
        if self._thisptr!=NULL:
            del self._thisptr
    cpdef void insert(self, int k, int v):
        self._thisptr.insert(k, v)
    cpdef void printTree(self):
        self._thisptr.printTree()
