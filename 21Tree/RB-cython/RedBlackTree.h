
#ifndef RED_BLACK_TREE_H
#define RED_BLACK_TREE_H

const int RED = 1;
const int BLACK = 2;

template <typename KEY, typename VALUE>
class RedBlackTree {
public:
    struct rbNode{
        KEY key;
        VALUE value;
        rbNode *left, *right, *parent;
        int color;
        
        rbNode (KEY k, VALUE v);
        rbNode ();
    } *root, *x, *y, *now;
    
    rbNode *NIL;
    
    RedBlackTree ();
    ~RedBlackTree();
    
    void insert (KEY key, VALUE value);
    void printTree ();
    void printTree (rbNode *n);
    void clear();
    void clear (rbNode *n);
    rbNode* search (KEY key);
    void left_rotate(rbNode *x);
    // copy from left_rotate but change x to y, left to right and vice versa
    void right_rotate(rbNode *y);
    void insert_fix(rbNode *x);
    int depth();
    int depth (rbNode *n);
};

#include <iostream>
#include "RedBlackTree.h"

using namespace std;

template <typename KEY, typename VALUE>
RedBlackTree<KEY, VALUE>::rbNode::rbNode(KEY k, VALUE v)
{
    key = k;
    value = v;
    left = right = parent = NULL;
    color = RED;
}
template <typename KEY, typename VALUE>
RedBlackTree<KEY, VALUE>::rbNode::rbNode()
{
    left = right = parent = NULL;
    color = BLACK;
}
template <typename KEY, typename VALUE>
RedBlackTree<KEY, VALUE>::RedBlackTree()
{
    NIL = new rbNode();
    root = NIL;
}
template <typename KEY, typename VALUE>
RedBlackTree<KEY, VALUE>::~RedBlackTree()
{
    clear();
    delete (NIL);
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::insert (KEY key, VALUE value)
{
    now = new rbNode (key, value);
    now->left = now->right = now->parent = NIL;
    
    x = root;
    y = NIL;
    
    while (x!=NIL){
        y = x;
        if (now->key < x->key){
            x = x->left;
        }
        else if (now->key > x->key) {
            x = x->right;
        }
        else{
            x->value = value;
            return ;
        }
    }
    
    if (y==NIL){
        root = now;
    }
    else if (now->key <  y->key){
        y->left = now;
    }
    else{
        y->right = now;
    }
    
    now->parent = y;
    
    insert_fix(now);
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::printTree ()
{
    printTree(root);
    cout<<endl;
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::printTree (rbNode *n)
{
    if (n == NIL)return;
    printTree (n->left);
    cout<<n->key<<" "<<n->value<<" "<<n->color<<endl;
    printTree (n->right);
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::clear()
{
    clear(root);
    root = NIL;
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::clear (rbNode *n)
{
    if (n == NIL)return;
    clear (n->left);
    clear (n->right);
    delete (n);
}
template <typename KEY, typename VALUE>
typename RedBlackTree<KEY, VALUE>::rbNode* RedBlackTree<KEY, VALUE>::search (KEY key)
{
    x = root;
    
    while (x!=NIL){
        if (key < x->key){
            x = x->left;
        }
        else if (key > x->key){
            x = x->right;
        }
        else break;
    }
    
    return x;
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::left_rotate(rbNode *x)
{
    //make y
    y = x->right;
    
    //connect x->right to b
    x->right = y->left;
    if (x->right!=NIL){
        x->right->parent = x;
    }
    
    //connect y to parent's x
    y->parent = x->parent;
    if (x->parent == NIL){
        root = y;
    }
    else if (x->parent->left == x){
        x->parent->left = y;
    }
    else{
        x->parent->right = y;
    }
    
    //connect y to x
    y->left = x;
    x->parent = y;
}

// copy from left_rotate but change x to y, left to right and vice versa
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::right_rotate(rbNode *y)
{
    x = y->left;
    
    y->left = x->right;
    if (y->left!=NIL){
        y->left->parent = y;
    }
    
    x->parent = y->parent;
    if (y->parent == NIL){
        root = x;
    }
    else if (y->parent->left == y){
        y->parent->left = x;
    }
    else{
        y->parent->right = x;
    }
    
    x->right = y;
    y->parent = x;
}
template <typename KEY, typename VALUE>
void RedBlackTree<KEY, VALUE>::insert_fix(rbNode *x)
{
    while (x->parent->color == RED){
        if (x->parent->parent->left == x->parent){
            y = x->parent->parent->right;
            if (y->color == RED){
                y->color = BLACK;
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                x = x->parent->parent;
            }else{
                if (x->parent->right == x){
                    x = x->parent;
                    left_rotate(x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                right_rotate(x->parent->parent);
            }
        }
        else{
            y = x->parent->parent->left;
            if (y->color == RED){
                y->color = BLACK;
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                x = x->parent->parent;
            }else{
                if (x->parent->left == x){
                    x = x->parent;
                    right_rotate(x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                left_rotate(x->parent->parent);
            }
        }
    }
    root->color = BLACK;
}
template <typename KEY, typename VALUE>
int RedBlackTree<KEY, VALUE>::depth()
{
    return depth(root);
}
template <typename KEY, typename VALUE>
int RedBlackTree<KEY, VALUE>::depth (rbNode *n)
{
    if (n==NIL)return 0;
    return 1 + max(depth(n->left), depth(n->right));
}

#endif
