#include <iostream>
#include "RedBlackTree.h"

using namespace std;

int main (){
    RedBlackTree <int,int > rbt;
    rbt.insert(1,2);
    rbt.insert(2,3);
    rbt.insert(3,6);
    rbt.insert(4,7);
    rbt.insert(5,5);
    rbt.insert(6,2);
    rbt.insert(7,8);
    
    rbt.print();
    
    cout<<rbt.depth()<<endl;
    cin.get();
	return 0;
}
