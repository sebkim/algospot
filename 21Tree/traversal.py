
def printPostOrder(preOrd, inOrd):
	if len(preOrd) == 0:
		return
	rootElem = preOrd[0]
	leftTreeSize = rootIndInInorder = inOrd.index(rootElem)
	# print left, right , and rootElem
	printPostOrder(preOrd[1:1+leftTreeSize], inOrd[:leftTreeSize])
	printPostOrder(preOrd[1+leftTreeSize:], inOrd[leftTreeSize+1:])
	ans.append(rootElem)

import sys

rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	lenData = int(rl().strip())
	preOrd = [int(i) for i in rl().strip().split()]
	inOrd = [int(i) for i in rl().strip().split()]
	ans = []
	printPostOrder(preOrd, inOrd)
	print ' '.join((str(i) for i in ans))
