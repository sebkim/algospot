class treap(object):
	def __init__(self, key = None):
		import sys
		import random
		self.key = key
		self.priority = random.randint(0, sys.maxint)
		self.left = None
		self.right = None
		self.size  = 1
		self.parent = None
	def setLeft(self, newLeft):
		self.left = newLeft
		self.calcSize()
	def setRight(self, newRight):
		self.right = newRight
		self.calcSize()
	def calcSize(self):
		self.size=1
		if self.left: self.size+=self.left.size
		if self.right: self.size+=self.right.size
	def height(self):
		lh = self.left.height() if self.left else 0
		rh = self.right.height() if self.right else 0
		return max(lh, rh) +1
	def __str__(self, level=0):
		hStr = "\t"*level + repr(self.priority) + " " + repr(self.key)
		if self.parent is not None: hStr+="   " + repr(self.parent.key)
		hStr += "\n"
		if self.left:
			hStr += self.left.__str__(level+1)
		else: hStr += "\t"*(level+1) + "LeftNone\n"
		if self.right:
			hStr += self.right.__str__(level+1)
		else: hStr += "\t"*(level+1) + "RightNone\n"
		return hStr

def split(root, key):
	if root is None:
		return (None, None)
	if root.key < key:
		nodepair = split(root.right, key)
		if nodepair[0] is not None: nodepair[0].parent = root
		root.setRight(nodepair[0])
		return (root, nodepair[1])
	nodepair = split(root.left, key)
	if nodepair[1] is not None: nodepair[1].parent = root
	root.setLeft(nodepair[1])
	return (nodepair[0], root)
def insert(root, newNode, parentParam=None):
	if root is None:
		newNode.parent = parentParam
		return newNode
	if root.priority < newNode.priority:
		splitted = split(root, newNode.key)
		if splitted[0] is not None: splitted[0].parent = newNode
		if splitted[1] is not None: splitted[1].parent = newNode
		newNode.setLeft(splitted[0])
		newNode.setRight(splitted[1])
		return newNode
	elif root.key > newNode.key:
		root.setLeft(insert(root.left, newNode, root))
	else:
		root.setRight(insert(root.right, newNode, root))
	return root
def merge(root, mergedNode):
	if root is None: return mergedNode
	if mergedNode is None: return root
	if root.priority < mergedNode.priority:
		mer = merge(root, mergedNode.left)
		if mer: mer.parent = mergedNode
		mergedNode.setLeft(mer)
		return mergedNode
	mer = merge(root.right, mergedNode)
	if mer: mer.parent  = root
	root.setRight(mer)
	return root
def erase(root, key):
	if root is None: return None
	if root.key == key:
		retNode = merge(root.left, root.right)
		if retNode: retNode.parent = root.parent
		return retNode
	if root.key > key:
		root.setLeft(erase(root.left, key))
	else:
		root.setRight(erase(root.right, key))
	return root

t = None
for i in range(1, 13):
	t = insert(t, treap(i))

t = erase(t, 5)
t = erase(t, 6)

print t
print t.height()
print t.size