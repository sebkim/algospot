from __future__ import print_function
class TreeNode(object):
	def __init__(self, elem=None):
		self.elem = elem
		self.children = []
		self.parent = None

def sqrDist(a, b):
	return pow(castleDat[a][0] - castleDat[b][0], 2) + pow(castleDat[a][1] - castleDat[b][1], 2)
def encloses(a, b):
	if castleDat[a][2] > castleDat[b][2] and \
	sqrDist(a, b) < pow( (castleDat[a][2] - castleDat[b][2]), 2):
		return True
	return False
# def isChildNaive(parent, child):
# 	if not encloses(parent, child): return False
# 	for eachCastle in range(nCastle):
# 		if eachCastle != parent and eachCastle != child and \
# 		encloses(parent, eachCastle) and encloses(eachCastle, child):
# 			return False
# 	return True
# def getTreeNaive(root, whoCall):
# 	ret = TreeNode(root)
# 	ret.parent = whoCall
# 	for i in range(nCastle):
# 		if isChildNaive(root, i):
# 			ret.children.append(getTreeNaive(i, root))
# 	return ret
# def printTree(rootNode):
# 	print (rootNode.elem, rootNode.parent)
# 	for i in range(len(rootNode.children)):
# 		printTree(rootNode.children[i])

def getParentList():
	# rootNode = TreeNode(0)
	# rootNode.parent = -1
	parentList = [-1 for i in range(nCastle)]
	for childPossible in range(0, nCastle-1):
		for parentPossible in range(childPossible+1, nCastle):
			if encloses(parentPossible, childPossible):
				parentList[childPossible] = parentPossible
				break
	return parentList
def getTree(root, whoCall):
	node = TreeNode(root)
	node.parent = whoCall
	myChildren = [ (i,j) for i,j in enumerate(parentList) if j==root ]
	for i in range(len(myChildren)):
		node.children.append(getTree(myChildren[i][0], root))
	return node

def getHeight(rootNode):
	global longest
	childFirstMaxHeight, childSecondMaxHeight = -1, -1
	for i in range(len(rootNode.children)):
		childHeight = getHeight(rootNode.children[i])
		if childFirstMaxHeight < childHeight:
			childSecondMaxHeight = childFirstMaxHeight
			childFirstMaxHeight = childHeight
		elif childSecondMaxHeight < childHeight:
			childSecondMaxHeight = childHeight
	if len(rootNode.children) == 0: return 0
	# leaf to leaf path update
	if childFirstMaxHeight >=0 and childSecondMaxHeight>=0:
		longest = max(longest, 2+ childFirstMaxHeight + childSecondMaxHeight)
	##
	return childFirstMaxHeight+1

def solve(rootNode):
	maxHeight = getHeight(rootNode)
	return max(maxHeight, longest)

import sys

rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nCastle = int(rl().strip())
	castleDat = []
	for i in range(nCastle):
		oneLine = [int(j) for j in rl().strip().split()]
		castleDat.append(oneLine)
	# castleDat = [castleDat[0]] + sorted(castleDat[1:], key=lambda x:x[2])
	castleDat = sorted(castleDat, key=lambda x:x[2])
	parentList = getParentList()
	root = len(castleDat)-1
	longest = 0
	rootNode = getTree(root, -1)
	print (solve(rootNode))
