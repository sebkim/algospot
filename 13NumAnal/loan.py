
def balance(amount, duration, curBal, interest):
	remaining = curBal
	for i in range(duration):
		remaining *= 1+0.01*interest/12.
		remaining -= amount
	return remaining
def loan(duration, curBal, interest):
	lo = 0.; hi= curBal * (1+0.01*interest/12.) + 1
	for it in range(100):
		mid = (lo+hi) / 2.
		if balance(mid, duration, curBal, interest) <=0:
			hi = mid
		else:
			lo = mid
	return hi

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	oneLine = [i for i in rl().strip().split()]
	curBal = float(oneLine[0])
	duration = int(oneLine[1])
	interest = float(oneLine[2])
	print ("{:.8f}".format(loan(duration, curBal, interest)))
