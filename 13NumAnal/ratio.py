
def ratio(a, b):
	return int(a*100/float(b))
def neededGames():
	if ratio(nWin+L, nPlay+L) == ratio(nWin, nPlay):
		return -1
	lo = 0; hi=L+1;
	while lo+1<hi:
		mid = (lo+hi)//2
		# print ratio(nWin, nPlay), ratio(nWin+mid, nPlay+mid), lo, hi, mid
		if ratio(nWin, nPlay) == ratio(nWin+mid, nPlay+mid):
			lo = mid
		else:
			hi = mid
	return hi

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nPlay, nWin = [int(i) for i in rl().strip().split()]
	L = pow(10,9)*2
	print (neededGames())
