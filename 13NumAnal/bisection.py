import numpy as np
import matplotlib.pyplot as plt
x = np.arange(-10, 10, 0.01)
def myf(x):
    return 3*x*x -12*x - 10
y = myf(x)

def bisection(x1, x2):
    y1 = myf(x1)
    y2 = myf(x2)
    if y1*y2>0: return False
    if y1>y2:
        y1, y2 = y2, y1
        x1, x2 = x2, x1
    for itera in xrange(100):
        midx = (x1+x2)/2.0
        midy = myf(midx)
        if y1*midy>0:
            x1 = midx
            y1 = midy
        else:
            x2 = midx
            y2 = midy
    return (x1+x2)/2.0
            
ans = bisection(-10, 3)
print ans
print myf(ans)


# plt.plot(x,y)
# plt.show()
