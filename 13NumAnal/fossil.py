
def decompose(hull):
	for onePointInd, onePoint in enumerate(hull):
		if hull[onePointInd][0] < hull[(onePointInd+1)%len(hull)][0]:
			lower.append((hull[onePointInd], hull[(onePointInd+1)%len(hull)]))
		else:
			upper.append((hull[onePointInd], hull[(onePointInd+1)%len(hull)]))
def between(point1, point2, x):
	return point1[0] <= x <= point2[0] or point2[0] <= x <= point1[0]
def at(point1, point2, x):
	dy = point2[1] - point1[1]; dx = point2[0] - point1[0];
	# point1.y + (x-point1.x) * dy /dx
	return point1[1] + (x-point1[0]) * dy / dx
def vertical(x):
	minUp = float('inf')
	maxLow = -float('inf')
	for eachUpper in upper:
		if between(eachUpper[0], eachUpper[1], x):
			minUp = min(minUp, at(eachUpper[0], eachUpper[1], x))
	for eachLower in lower:
		if between(eachLower[0], eachLower[1], x):
			maxLow = max(maxLow, at(eachLower[0], eachLower[1], x))
	return minUp - maxLow
def fossil():
	minHull1 = min([i[0] for i in hull1])
	minHull2 = min([i[0] for i in hull2])
	maxHull1 = max([i[0] for i in hull1])
	maxHull2 = max([i[0] for i in hull2])
	lo = max(minHull1, minHull2)
	hi = min(maxHull1, maxHull2)
	if lo>hi:
		return 0
	for it in range(100):
		aa = (2*lo+hi) / 3.
		bb = (lo+2*hi) / 3.
		if vertical(aa) > vertical(bb):
			hi = bb
		else:
			lo = aa
	return max(0, vertical((lo+hi)/2.))
	# return vertical((lo+hi)/2.), (lo+hi)/2.

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nHull1, nHull2 = [int(i) for i in rl().strip().split()]
	hulls = []
	for i in range(2):
		oneLine = [float(i) for i in rl().strip().split()]
		oneHull = []
		for j in range(len(oneLine)/2):
			onePoint = [oneLine[2*j], oneLine[2*j+1]]
			oneHull.append(onePoint)
		hulls.append(oneHull)
	hull1 = hulls[0]; hull2 = hulls[1];
	upper = []
	lower = []
	decompose(hull1)
	decompose(hull2)
	print fossil()
