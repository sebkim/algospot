
import math
def evaluate(order, terms, x):
	ret = 0
	for i in range(order+1):
		ret += terms[order-i] * pow(x, i)
	return ret
def differentiate(order, terms):
	newTerms = []
	for i in range(order+1):
		if i==0: continue
		newTerms.append(terms[order-i] * i)
	newTerms = list(reversed(newTerms))
	return newTerms
# for solving second order polynomial
def naiveSolve(order, terms):
	a = terms[0]; b = terms[1]; c=terms[2];
	upSquarePart = math.sqrt( pow(b, 2) - 4*a*c )
	return [ (-b-upSquarePart) / (2*a), (-b+upSquarePart) / (2*a) ]
def roots(order, terms):
	if order<=2:
		return naiveSolve(order, terms)
	diffTerms = differentiate(order, terms)
	sols=[-L]
	sols += roots( len(diffTerms)-1, diffTerms)
	sols+=[L]
	ret = []
	for eachSolInd, eachSol in enumerate(sols[:-1]):
		x1 = sols[eachSolInd]; x2 = sols[eachSolInd+1];
		y1 = evaluate(order, terms, x1); y2 = evaluate(order, terms, x2);
		if y1*y2 > 0: continue
		if y1>y2:
			y1, y2 = y2, y1
			x1, x2 = x2, x1
		for it in range(100):
			midX = (x1+x2) / 2.
			midY = evaluate(order, terms, midX)
			if midY == 0:
				break
			elif midY * y1 > 0:
				x1 = midX
				y1 = midY
			else:
				x2 = midX
				y2 = midY
		ret.append( (x1+x2) / 2.0 )
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	order = int(rl().strip())
	terms  = [float(i) for i in rl().strip().split()]
	L=11.
	print " ".join(["{:.9f}".format(i) for i in roots(order, terms)])
