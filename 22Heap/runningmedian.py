
MOD = 20090711
def RGN(nData, a, b):
	ret = []
	nextVal = 1983
	ret.append(nextVal)
	for i in range(1, nData):
		nextVal = (nextVal * a + b) % MOD
		ret.append(nextVal)
	return ret

def runningMedian(data):
	minHeap = []
	maxHeap = []
	maxTurn = True
	ans = 0
	for eachDat in data:
		if maxTurn:
			heapq.heappush(maxHeap, -eachDat)
		else:
			heapq.heappush(minHeap, eachDat)
		maxTurn = not maxTurn
		if len(maxHeap)>0 and len(minHeap)>0 and -maxHeap[0] > minHeap[0]:
			a, b = maxHeap[0], minHeap[0]
			heapq.heappop(minHeap); heapq.heappop(maxHeap); heapq.heappush(minHeap, -a); heapq.heappush(maxHeap, -b);
		ans += -maxHeap[0]
	return ans

import sys
import heapq

rl = lambda: sys.stdin.readline()
n = int(rl())
# n=1
for eachCase in range(n):
	nData, a, b = [int(i) for i in rl().strip().split()]

	# nData = 5
	# data = [5,3,2,7,6]

	data = RGN(nData, a, b)
	print runningMedian(data) % MOD
