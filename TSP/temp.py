class UnionFind(object):
	def __init__(self, n):
		self.parent = [i for i in range(n)]
		self.rank = [1 for i in range(n)]
	def find(self, u):
		if u==self.parent[u]: return u
		self.parent[u] = self.find(self.parent[u])
		return self.parent[u]
	def merge(self, u, v):
		u = self.find(u); v = self.find(v);
		if u==v: return
		if self.rank[u] > self.rank[v]: u, v = v, u
		self.parent[u] = v
		if self.rank[u] == self.rank[v]: self.rank[v]+=1
		return True

def mstHeuristic(path, visited):
	taken = 0
	sets = UnionFind(n)
	for eachEdge in zip([None] + path, path + [None]):
		if None not in eachEdge:
			sets.merge(eachEdge[0], eachEdge[1])
	for i in range(len(edges)):
		a = edges[i][1][0]; b = edges[i][1][1];
		# if visited[a] and visited[b]: continue
		if (visited & (1<<a)) == (visited & (1<<b)) == 0: continue
		if sets.merge(a, b):
			taken += edges[i][0]
			print sets.parent, a, b
	print taken, visited
	return taken

path = [0]
visited = (1<<0)
n = 3
# edges = [[[0, [None, None]] for i in range(n)] for j in range(n)]
edges = []
dist = [[1 if i!=j else 0 for i in range(n)] for j in range(n)]
for i in range(n):
	for j in range(n):
		edges.append([dist[i][j], [i, j]])
edges.sort()
print edges
mstHeuristic(path, visited)
