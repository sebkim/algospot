#cython: profile = True

from cython cimport boundscheck, wraparound

cdef inline int inlineMin(int x, int y):
	if x<y:
		return x
	else:
		return y

def dpWrap(int[:,::1] dist, int[:,::1] cached, int n, int here, int visited):
	return dp(dist, cached, n, here, visited)

@boundscheck(False)
@wraparound(False)
cdef int dp(int[:,::1] dist, int[:,::1] cached, int n, int here,int visited):
	cdef int next
	if visited == (1<<n)-1: return dist[here, 0]
	if cached[here, visited] != -1:
		return cached[here, visited]
	cdef int ret = 987654321
	for next in xrange(n):
		if visited & (1<<next): continue
		ret = inlineMin(ret, dp(dist, cached, n, next, visited | (1<<next)) + dist[here, next])
	cached[here, visited] = ret
	return ret
