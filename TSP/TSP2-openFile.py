def dp(here, visited):
	if visited == (1<<n)-1: return dist[here][0]
	if cached[here][visited] is not None:
		return cached[here][visited]
	ret = float('inf')
	for next in range(n):
		if visited & (1<<next): continue
		ret = min(ret, dp(next, visited | (1<<next)) + dist[here][next])
	cached[here][visited] = ret
	return ret

import sys
f = open(sys.argv[1], 'r')
f = f.readlines()
f = [i.strip().split() for i in f]
f = [ [int(j) for j in i ]for i in f]
dist = f
n = len(f[0])
cached = [[None for i in range(1<<n)] for j in range(n)]
print dp(0, 1<<0)
