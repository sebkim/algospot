import pyximport; pyximport.install();
from TSP3 import dpWrap
import numpy as np
import sys

f = open(sys.argv[1], 'r')
f = f.readlines()
f = [i.strip().split() for i in f]
f = [ [int(j) for j in i ]for i in f]
dist = f
dist = np.asarray(dist, dtype=np.int32)
n = len(f[0])
# cached = [[-1 for i in range(1<<n)] for j in range(n)]
# cached = np.asarray(cached, dtype=np.int32)
cached = np.full((n, 1<<n), -1, dtype=np.int32)
print dpWrap(dist, cached, n, 0, 1<<0)
