class UnionFind(object):
	def __init__(self, n):
		self.parent = [i for i in range(n)]
		self.rank = [1 for i in range(n)]
	def find(self, u):
		if u==self.parent[u]: return u
		self.parent[u] = self.find(self.parent[u])
		return self.parent[u]
	def merge(self, u, v):
		u = self.find(u); v = self.find(v);
		if u==v: return
		if self.rank[u] > self.rank[v]: u, v = v, u
		self.parent[u] = v
		if self.rank[u] == self.rank[v]: self.rank[v]+=1

n=5
sets = UnionFind(n)
sets.merge(0,3)
sets.merge(1,2)
print sets.parent

import itertools
for k, g in itertools.groupby(sorted(zip(sets.parent, range(n))), key=lambda x:x[0]):
	print k
	print list(g)