class UnionFind(object):
	def __init__(self, n):
		self.parent = [i for i in range(n)]
		self.rank = [1 for i in range(n)]
	def find(self, u):
		if u==self.parent[u]: return u
		self.parent[u] = self.find(self.parent[u])
		return self.parent[u]
	def merge(self, u, v):
		u = self.find(u); v = self.find(v);
		if u==v: return
		if self.rank[u] > self.rank[v]: u, v = v, u
		self.parent[u] = v
		if self.rank[u] == self.rank[v]: self.rank[v]+=1
		return True


def search(path, visited, currentLength):
	global best
	here = path[-1]
	if len(path) + CACHED_DEPTH >= n:
		best = min(best, currentLength + dp(path[-1], visited))
		return
	if pathReversePruning(path):
		return
	# if best <= currentLength + simpleHeuristic(visited):
	if best <= currentLength + mstHeuristic(path, visited):
		return
	if len(path) == n:
		best = min(best, currentLength + dist[here][0])
	for next in nearest[here]:
		# if visited[next] is False:
		if (visited & (1<<next)) == 0:
			path.append(next)
			# visited[next] = True
			visited = visited | (1<<next)
			search(path, visited, currentLength + dist[here][next])
			# visited[next] = False
			visited = visited ^ (1<<next)
			path.pop()
def solve():
	global best
	best = float('inf')
	# visited = [False for i in range(n)]
	# visited[0] = True
	visited = 1<<0
	path = [0]
	# minEdge init
	for i in range(n):
		minEdge[i] = float('inf')
		for j in range(n):
			if i!=j:
				minEdge[i] = min(minEdge[i], dist[i][j])
	###
	# nearest init
	for i in range(n):
		order = []
		for j in range(n):
			if i!=j:
				order.append((dist[i][j], j))
		order.sort()
		for j in range(n-1):
			nearest[i].append(order[j][1])
	###
	# edges init
	for i in range(n):
		for j in range(n):
			edges.append((dist[i][j], (i, j)))
	edges.sort()
	###
	search(path, visited, 0)
	return best

def simpleHeuristic(visited):
	ret = minEdge[0] # last edge to starting point.
	for i in range(n):
		# if visited[i] is False:
		if (visited & (1<<i)) == 0:
			ret += minEdge[i]
	return ret
def mstHeuristic(path, visited):
	taken = 0
	sets = UnionFind(n)
	for i in range(len(edges)):
		a = edges[i][1][0]; b = edges[i][1][1];
		if a!=0 and a!=path[-1] and (visited & (1<<a)): continue
		if b!=0 and b!=path[-1] and (visited & (1<<b)): continue
		if sets.merge(a,b):
			taken+=edges[i][0]
	return taken

def pathReversePruning(path):
	if len(path) < 4: return False
	b = path[-2]
	q = path[-1]
	for i in range(len(path)-3):
		p = path[i]
		a = path[i+1]
		# .., p, a, ..., b, q and ..., p,b, ..., a, q (try swapping)
		if dist[p][a] + dist[b][q] > dist[p][b] + dist[a][q]:
			return True
	return False

def dp(here, visited):
	if visited == (1<<n)-1: return dist[here][0]
	remaining = n - len(filter(lambda x:x=='1', bin(visited)[2:]))
	if cached[here][remaining].get(visited) is not None:
		return cached[here][remaining][visited]
	ret = float('inf')
	for next in range(n):
		if visited & (1<<next): continue
		ret = min(ret, dp(next, visited | (1<<next)) + dist[here][next])
	cached[here][remaining][visited] = ret
	return ret

import sys
rl = lambda: sys.stdin.readline()
dist = []
oneLine = [int(i) for i in rl().strip().split()]
oneLine = [i if i!=0 else float('inf') for i in oneLine]
dist.append(oneLine)
n = len(oneLine)
for i in range(1, n):
	oneLine = [int(i) for i in rl().strip().split()]
	oneLine = [i if i!=0 else float('inf') for i in oneLine]
	dist.append(oneLine)
best = None
minEdge = [None for i in range(n)]
nearest = [[] for i in range(n)]
edges = []
CACHED_DEPTH = 5
cached = [[{} for i in range(CACHED_DEPTH+1)] for j in range(n)]
print solve()
