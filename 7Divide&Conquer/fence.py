
def fence(left, right):
	if left == right:
		return h[left]
	mid = (left + right) / 2
	ret = max(fence(left, mid), fence(mid+1, right))	
	# if max is contained in the range(mid, mid+1+1)
	lo = mid; hi = mid+1
	height = min(h[lo], h[hi])
	ret = max(ret, height*2)
	# if lo==left and hi==right:
	# 	pass
	# else:
	# 	while lo<hi and lo>=left and hi<=right:
	# 		notCommitedLo =  lo-1; notCommitedHi = hi+1
	# 		if notCommitedLo<left or notCommitedHi>right:
	# 			break
	# 		bottomLen+=1
	# 		if h[lo] >= h[hi]:
	# 			lo = notCommitedLo
	# 		else:
	# 			hi = notCommitedHi
	# 		hereH = min(h[lo:hi+1]) * bottomLen
	# 		if maxH < hereH:
	# 			maxH = hereH
	# 	if lo==left and hi==right:
	# 		pass
	# 	else:
	# 		if hi==right:
	# 			while lo>left:
	# 				bottomLen+=1
	# 				lo-=1
	# 				hereH = min(h[lo:hi+1]) * bottomLen
	# 				if maxH < hereH:
	# 					maxH = hereH
	# 		elif lo==left:
	# 			while hi<right:
	# 				bottomLen+=1
	# 				hi+=1
	# 				hereH = min(h[lo:hi+1]) * bottomLen
	# 				if maxH < hereH:
	# 					maxH = hereH
	while left < lo or right > hi:
		if right > hi and (lo==left or h[lo-1] < h[hi+1]):
			hi+=1
			height = min(height, h[hi])
		else:
			lo-=1
			height = min(height, h[lo])
		ret = max(ret, height * (hi-lo+1))
	return ret

import sys
from array import array

rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenH = int(rl().strip())
	# h = [ int(i) for i in rl().strip().split(' ') ]
	h = array('H', [ int(i) for i in rl().strip().split(' ') ])
	print fence(0, lenH-1)
