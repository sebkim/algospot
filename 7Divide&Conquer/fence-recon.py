
def fence(left, right, reconList):
	if left == right:
		reconList.append(('solo', h[left], left, right))
		return h[left]
	mid = (left + right) / 2
	leftMax = fence(left, mid, reconList)
	rightMax = fence(mid+1, right, reconList)
	# whichOneIsMax: 0 is left, 1 is right, and 2 is middle
	whichOneIsMax = -1
	if leftMax < rightMax:
		whichOneIsMax = 1
		ret = rightMax
	else:
		whichOneIsMax = 0
		ret = leftMax
	# if max is contained in the range(mid, mid+1+1)
	lo = mid; hi = mid+1
	height = min(h[lo], h[hi])
	if ret < height*2:
		ret = height*2
		whichOneIsMax = 2
		bestlo = lo; besthi=hi;
	while left < lo or right > hi:
		if right > hi and (lo==left or h[lo-1] < h[hi+1]):
			hi+=1
			height = min(height, h[hi])
		else:
			lo-=1
			height = min(height, h[lo])
		if ret < height*(hi-lo+1):
			ret = height*(hi-lo+1)
			whichOneIsMax = 2
			bestlo = lo; besthi=hi;
	if whichOneIsMax==0:
		reconList.append(('left', leftMax, left, right))
	elif whichOneIsMax==1:
		reconList.append(('right', rightMax, left, right))
	else:
		reconList.append(('middle', ret, left, right, bestlo, besthi))
	return ret

def printReadableRecon(left, right):
	mid = (left + right) /2
	info = reconDic[(left, right)]
	if info[0] == 'middle':
		print left, right, 'middle', info[2], info[3], info[1]
	elif info[0] == 'left':
		printReadableRecon(left, mid)
	elif info[0] == 'right':
		printReadableRecon(mid+1, right)
	elif info[0] == 'solo':
		print left, right, 'solo', info[1]

import sys
from array import array

rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	lenH = int(rl().strip())
	# h = [ int(i) for i in rl().strip().split(' ') ]
	h = array('H', [ int(i) for i in rl().strip().split(' ') ])
	reconList = []
	print fence(0, lenH-1, reconList)
	reconDic = {}
	for eachRecon in reconList:
		if eachRecon[0] == 'middle':
			reconDic[(eachRecon[2], eachRecon[3])] = (eachRecon[0], eachRecon[1], eachRecon[4], eachRecon[5])
		else:
			reconDic[(eachRecon[2], eachRecon[3])] = (eachRecon[0], eachRecon[1])
	printReadableRecon(0, lenH-1)
