
def reverseQuad(quadrepIt):
	head = quadrepIt.next()
	if head=='b' or head=='w':
		return head
	upperLeft = reverseQuad(quadrepIt)
	upperRight = reverseQuad(quadrepIt)
	lowerLeft = reverseQuad(quadrepIt)
	lowerRight = reverseQuad(quadrepIt)
	return 'x' + lowerLeft + lowerRight + upperLeft + upperRight

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	quadrep = [ i for i in rl().strip().split(' ') ]
	quadrep = quadrep[0]
	print reverseQuad(iter(quadrep))
	
