from collections import deque
def partition(nums, lo, hi):
	pivot = nums[lo]
	leftWall = lo
	for i in range(lo+1, hi+1):
		if nums[i] < pivot:
			leftWall+=1
			nums[leftWall], nums[i] = nums[i], nums[leftWall]
	nums[leftWall], nums[lo] = nums[lo], nums[leftWall]
	return leftWall

def qsort(nums, lo, hi):
	if lo<hi:
		pivotInd = partition(nums, lo, hi)
		qsort(nums, lo, pivotInd-1)
		qsort(nums, pivotInd+1, hi)

def mergeSort(nums, lo, hi):
	if lo<hi:
		mid = (hi+lo) // 2
		mergeSort(nums, lo, mid)
		mergeSort(nums, mid+1, hi)
		merge(nums, lo, mid, hi)

def merge(nums, lo, mid, hi):
	buffer1 = deque(nums[lo:mid+1]); buffer2 = deque(nums[mid+1:hi+1]);
	ind = lo
	while not (len(buffer1)==0 or len(buffer2)==0):
		if buffer1[0] <= buffer2[0]:
			nums[ind] = buffer1.popleft()
		else:
			nums[ind] = buffer2.popleft()
		ind+=1
	while not len(buffer1)==0:
		nums[ind] = buffer1.popleft()
		ind+=1
	while not len(buffer2)==0:
		nums[ind] = buffer2.popleft()
		ind+=1
	print nums

nums = [3,2,1,0,7,4]
# qsort(nums, 0, len(nums)-1)
mergeSort(nums, 0, len(nums)-1)
print nums
