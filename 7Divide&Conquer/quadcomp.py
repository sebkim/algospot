
def reverseQuad(quadrepIt):
	head = quadrepIt.next()
	if head=='b' or head=='w':
		return head
	upperLeft = reverseQuad(quadrepIt)
	upperRight = reverseQuad(quadrepIt)
	lowerLeft = reverseQuad(quadrepIt)
	lowerRight = reverseQuad(quadrepIt)
	return 'x' + lowerLeft + lowerRight + upperLeft + upperRight

def compress(y, x, size):
	ret = ''
	if all(j=='w' for i in raw[y:y+size] for j in i[x:x+size]):
		return 'w'
	elif all(j=='b' for i in raw[y:y+size] for j in i[x:x+size]):
		return 'b'
	else:
		half = size//2
		ret += 'x'
		ret += compress(y, x, half)
		ret += compress(y, x+half, half)
		ret += compress(y+half, x, half)
		ret += compress(y+half, x+half, half)
	return ret

def decompress(quadrepIt, y, x, size):
	head = quadrepIt.next()
	if head=='b' or head=='w':
		for dy in range(size):
			for dx in range(size):
				decompressed[y+dy][x+dx] = head
	else:
		half = size // 2
		decompress(quadrepIt, y, x, half)
		decompress(quadrepIt, y, x+half, half)
		decompress(quadrepIt, y+half, x, half)
		decompress(quadrepIt, y+half, x+half, half)

import sys
rl = lambda: sys.stdin.readline()

raw = []
h, w = [int(i) for i in rl().strip().split()]
for i in range(w):
	oneLine = rl().strip()
	raw.append(oneLine)
print raw
compressed = compress(0, 0, w)
print compressed
decompressed = [[None for i in range(w)] for j in range(w)]
decompress(iter(compressed), 0, 0, w)
print decompressed
