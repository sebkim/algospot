import random, string
from array import array
from functools import cmp_to_key

def getSuffixArray(s):
	n = len(s)
	t=1
	group = array('i', [ord(s[i]) for i in range(n)])
	group.append(-1)
	perm = array('i', [i for i in range(n)])
	# group and t are required!!
	def compareUsing2T(a, b):
		if group[a] != group[b]: return group[a]-group[b]
		return group[a+t] - group[b+t]
	while t < n:
		perm = sorted(perm, key = cmp_to_key(compareUsing2T))
		newGroup = array('i', [0 for i in range(n+1)])
		newGroup[n] = -1
		newGroup[perm[0]] = 0
		for i in range(1, n):
			if compareUsing2T(perm[i-1], perm[i])<0:
				newGroup[perm[i]] = newGroup[perm[i-1]] + 1
			else:
				newGroup[perm[i]] = newGroup[perm[i-1]]
		group = newGroup
		t*=2
	return perm

def lenGreatestCommonPrefix(hereStr, i, j):
	k=0
	while i<len(hereStr) and j<len(hereStr) and hereStr[i] == hereStr[j]:
		i+=1; j+=1; k+=1;
	return k
def habit(testStr, K):
	suffixArr = getSuffixArray(testStr)
	ret = 0
	for eachSuffixInd in range(0, len(suffixArr) - K + 1):
		ret = max(ret, lenGreatestCommonPrefix(testStr, suffixArr[eachSuffixInd], suffixArr[eachSuffixInd+K-1]))
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	K = int(rl().strip())
	testStr = rl().strip()
	print habit(testStr, K)
