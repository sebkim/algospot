
class Node(object):
	def __init__(self, val):
		self.val = val
		self.next = None
		self.prev = None
# after insert, return newLastNode
def insert(lastNode, newNode):
	global root, llSize
	llSize+=1
	if lastNode is None:
		newNode.next = newNode
		newNode.prev = newNode
		root = newNode
		return newNode
	else:
		newNode.prev = lastNode
		newNode.next = lastNode.next
		lastNode.next = newNode
		newNode.next.prev = newNode
		return newNode
# after erase, return deleted node
def erase(thisNode):
	global root, llSize
	llSize-=1
	thisNode.prev.next = thisNode.next
	thisNode.next.prev = thisNode.prev
	if thisNode == root:
		if llSize==0: root = None
		else: root = root.next
	return thisNode

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nPer, k = [int(i) for i in rl().strip().split()]
	root = None
	lastNode = None
	llSize = 0
	for i in range(1, nPer+1):
		lastNode = insert(lastNode, Node(i))
	killNode = root
	while llSize > 2:
		cur = erase(killNode).next;
		for i in range(k-1):
			cur=cur.next
		killNode = cur
	ret = []
	cur = root
	for i in range(2):
		ret.append(cur.val)
		cur=cur.next
	ret.sort()
	print " ".join([str(i) for i in ret])
