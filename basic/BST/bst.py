# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x=None, parent = None):
		self.val = x
		self.left = None
		self.right = None
		self.parent = parent
		self.size = 1
	def setLeft(self, newLeft):
		self.left = newLeft
		self.calcSize()
	def setRight(self, newRight):
		self.right = newRight
		self.calcSize()
	def calcSize(self):
		self.size=1
		if self.left: self.size+=self.left.size
		if self.right: self.size+= self.right.size
	def __str__(self, level=0):
		ret = "\t"*level+repr(self.val)+', '
		if self.parent:
			ret += ', '+ repr(self.parent.val)
		ret += '\n'
		if self.left:
			ret += self.left.__str__(level=level+1)
		else:
			ret += "\t"*(level+1) + 'Left None.\n'
		if self.right:
			ret += self.right.__str__(level=level+1)
		else:
			ret += "\t"*(level+1) + 'Right None.\n'
		return ret
	def search(self, x):
		if x < self.val:
			if self.left is None:
				return None
			return self.left.search(x)
		elif x > self.val:
			if self.right is None:
				return None
			return self.right.search(x)
		else:
			return self
	def successor(self):
		if self.right is not None:
			succ = self.right
			while succ.left:
				succ = succ.left
			return succ
		# search up direction
		else:
			curNode = self
			if curNode.parent is None:
				return None
			while curNode.parent is not None and curNode.parent.right == curNode:
				curNode = curNode.parent
			if curNode.parent is not None and curNode.parent.left == curNode:
				return curNode.parent
			else:
				return None
	def predessor(self):
		if self.left is not None:
			succ = self.left
			while succ.right:
				succ = succ.right
			return succ
		# search up direction
		else:
			curNode = self
			if curNode.parent is None:
				return None
			while curNode.parent is not None and curNode.parent.left == curNode:
				curNode = curNode.parent
			if curNode.parent is not None and curNode.parent.right == curNode:
				return curNode.parent
			else:
				return None
	def children_count(self):
		cnt = 0
		if self.left:
			cnt+=1
		if self.right:
			cnt+=1
		return cnt
	def delete(self, x):
		target = self.search(x)
		if target is not None:
			sizeChildren = target.children_count()
			if sizeChildren == 0:
				if target.parent is None:
					self.val = None
				else:
					if target.parent.left is target:
						target.parent.left = None
					if target.parent.right is target:
						target.parent.right = None
				del target
			if sizeChildren == 1:
				if target.left:
					targetBottom = target.left
				if target.right:
					targetBottom = target.right
				if target.parent is None:
					self.val = targetBottom.val
					self.left = targetBottom.left
					self.right = targetBottom.right
				else:
					if target.parent.right is target:
						target.parent.right = targetBottom
					if target.parent.left is target:
						target.parent.left = targetBottom
				del target
			if sizeChildren == 2:
				succ = target.successor()
				succVal = succ.val
				self.delete(succVal)
				target.val = succVal
def lower_bound(node, key, lb = None):
	if node:
		if node.val >= key:
			return lower_bound(node.left, key, node)
		else:
			return lower_bound(node.right, key, lb)
	return lb
def insert(root, newNode, parentParam = None):
	if root is None:
		newNode.parent = parentParam
		return newNode
	if root.val > newNode.val:
		root.setLeft(insert(root.left, newNode, root))
	else:
		root.setRight(insert(root.right, newNode, root))
	return root
def merge(root, mergedNode):
	if root is None: return mergedNode
	if mergedNode is None: return root
	if root.size < mergedNode.size:
		mer = merge(root, mergedNode.left)
		if mer: mer.parent = mergedNode
		mergedNode.setLeft(mer)
		return mergedNode
	mer = merge(root.right, mergedNode)
	if mer: mer.parent = root
	root.setRight(mer)
	return root
def erase(root, val):
	if root is None: return None
	if root.val == val:
		retNode = merge(root.left, root.right)
		if retNode: retNode.parent = root.parent
		del root
		return retNode
	if root.val > val:
		root.setLeft(erase(root.left, val))
	else:
		root.setRight(erase(root.right, val))
	return root
bst = None
bst = insert(bst, TreeNode(5))
# for i in range(2, 13):
# 	bst = insert(bst, TreeNode(i))
bst = insert(bst, TreeNode(1))
bst = insert(bst, TreeNode(8))

# print lower_bound(bst, 4).val
bst = erase(bst, 5)
# bst.delete(5)
print bst
