def split(root, key):
	if root is None:
		return (None, None)
	if root.key < key:
		splitted = split(root.right, key)
		root.setRight(splitted[0])
		if splitted[0] is not None:
			splitted[0].parent = root
		return (root, splitted[1])
	splitted = split(root.left, key)
	root.setLeft(root, splitted[1])
	if splitted[1] is not None:
		splitted[1].parent = root
	return (splitted[0], root)

def insert(root, newNode, parentParam = None):
	if root is None:
		newNode.parent = parentParam
		return newNode
	if root.priority < newNode.priority:
		splitted = split(root, newNode.key)
		newNode.setLeft(splitted[0])
		if splitted[0] is not None: splitted[0].parent = newNode
		newNode.setRight(splitted[1])
		if splitted[1] is not None: splitted[1].parent = newNode
		newNode.parent = parentParam
		return newNode
	if newNode.key < root.key:
		root.setLeft(insert(root.left, newNode, root))
	else:
		root.setRight(insert(root.right, newNode, root))
		
def merge(a, b):
	if a is None: return b
	if b is None: return a
	if a.priority < b.priority:
		mer = merge(a, b.left)
		if mer is not None:
			mer.parent = b
		b.setLeft(mer)
		return b
	mer = merge(a.right, b)
	if mer is not None:
		mer.parent = a
	a.setRight(mer)
	return a

def erase(root, key):
	if root is None:
		return None
	if root.key == key:
		retNode = merge(root.left, root.right)
		if retNode is not None:
			retNode.parent = root.parent
		del root
		return retNode
	if key < root.key:
		nodeAfterErase = erase(root.left, key)
		root.setLeft(nodeAfterErase)
	else:
		nodeAfterErase = erase(root.right, key)
		root.setRight(nodeAfterErase)
	return root
