tmp = [[0, 1], [1,0], [1,2], [2,3], [3,1], [0,4], [4,5], [5,4]]
# tmp = [[0, 1], [1,0], [1,2], [2,3], [3,1], [0,4], [4,5], [5,4], [5,3]]
max_node_ind = max([max(i,j) for i,j in tmp])
adj = [[] for _ in xrange(max_node_ind+1)]
for each in tmp:
    adj[each[0]].append(each[1])
discovered = [None for _ in xrange(len(adj))]
scc_id = [None for _ in xrange(len(adj))]
stack = []
counter = -1
scc_counter = 0

# it returns the minimum discovered that here can reach
def scc(here):
    global counter, scc_counter
    counter += 1
    discovered[here] = counter
    ret = discovered[here]
    stack.append(here)
    for there in adj[here]:
        if discovered[there] is None:
            ret = min(ret, scc(there))
        # consider back or cross edge.
        elif scc_id[there] is None:
            ret = min(ret, discovered[there])
    if ret == discovered[here]:
        while True:
            tmp = stack.pop()
            scc_id[tmp] = scc_counter
            if tmp == here: break
        scc_counter+=1
    return ret

scc(0)
print scc_id
