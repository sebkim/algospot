tmp = [[0, 1], [1, 2], [0,2], [0,3], [3,4]]
adj = [[] for _ in xrange(len(tmp))]
for each in tmp:
    adj[each[0]].append(each[1])
    adj[each[1]].append(each[0])
discovered = [None for _ in xrange(len(adj))]
is_cut_vertex = [False for _ in xrange(len(adj))]
parent = [None for _ in xrange(len(adj))]
counter = -1
# it returns the minimum discovered that here can reach
def find_cut(here, is_root):
    global counter
    counter += 1
    discovered[here] = counter
    ret = discovered[here]
    children = 0
    for there in adj[here]:
        if discovered[there] is None:
            parent[there] = here
            children+=1
            subtree = find_cut(there, False)
            if not is_root and subtree >= discovered[here]:
                is_cut_vertex[here] = True
            ret = min(ret, subtree)
        elif parent[here] != there:
            ret = min(ret, discovered[there])
    if is_root:
        if children>=2: is_cut_vertex[here] = True
    return ret

find_cut(0, True)
print is_cut_vertex
print parent
