def getPartialMatch(needle):
	lNeedle = len(needle)
	pi = [0 for i in range(lNeedle)]
	begin = 1; matched = 0;
	while begin + matched < lNeedle:
		if needle[begin + matched] == needle[matched]:
			matched+=1
			pi[begin + matched - 1] = matched
		else:
			if matched == 0:
				begin+=1
			else:
				begin += matched - pi[matched-1]
				matched = pi[matched-1]
	return pi

def KMPsearch(haystack, needle):
	lHay = len(haystack); lNeedle = len(needle);
	ret = []
	pi = getPartialMatch(needle)
	begin = 0; matched = 0;
	while begin <= lHay - lNeedle:
		if matched < lNeedle and haystack[begin + matched] == needle[matched]:
			matched+=1
			if matched == lNeedle:
				ret.append(begin)
		else:
			if matched == 0:
				begin+=1
			else:
				begin += matched - pi[matched-1]
				matched = pi[matched-1]
	return ret

h = 'aabaabacxxxaaba'
n = 'aaba'

ans = KMPsearch(h, n)
for eachAns in ans:
	print h[eachAns:eachAns+len(n)]
