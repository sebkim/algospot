class unionFind(object):
	def __init__(self, n):
		self.parent = range(n)
		self.rank = [1 for i in range(n)]
	def find(self, key):
		if key==self.parent[key]: return key
		self.parent[key] = self.find(self.parent[key])
		return self.parent[key]
	def merge(self, a, b):
		a = self.find(a); b = self.find(b);
		if a==b: return None
		if self.rank[a] > self.rank[b]:
			a, b = b, a
		self.parent[a] = b
		if self.rank[a] == self.rank[b]:
			self.rank[b]+=1

x=unionFind(5)
x.merge(2,4)
x.merge(2,1)
print x.parent, x.rank
print x.find(1)
