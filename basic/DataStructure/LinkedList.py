
class Node(object):
	def __init__(self, val):
		self.val = val
		self.next = None
		self.prev = None
	def __str__(self):
		hStr = repr(self.val)
		if self.next and self.next is not root:
			hStr += "-->"
			hStr += self.next.__str__()
		return hStr
# after insert, return newLastNode
def insert(lastNode, newNode):
	global root, llSize
	llSize+=1
	if root is None:
		newNode.next = newNode
		newNode.prev = newNode
		root = newNode
		return newNode
	else:
		newNode.prev = lastNode
		newNode.next = lastNode.next
		lastNode.next = newNode
		newNode.next.prev = newNode
		return newNode
# after erase, return deleted node
def erase(thisNode):
	global root, llSize, lastNode
	llSize-=1
	thisNode.prev.next = thisNode.next
	thisNode.next.prev = thisNode.prev
	if thisNode == root:
		if llSize==0: root = None
		else: root = root.next
	if thisNode == lastNode:
		if llSize==0: lastNode = None
		else: lastNode = lastNode.prev
	return thisNode
def search(node, val):
	node = root
	while node:
		if node.val == val: return node
		node = node.next if node.next is not root else None
	return None

root = None
lastNode = None
llSize=0
for i in range(1, 6):
	lastNode = insert(lastNode, Node(i))
print root
recentNode = insert(search(root, 1), Node(100))
print root
erase(lastNode)
print root
