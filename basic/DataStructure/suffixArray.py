import random, string
from array import array
from functools import cmp_to_key

def getSuffixArray(s):
	n = len(s)
	t=1
	group = array('i', [ord(s[i]) for i in range(n)])
	group.append(-1)
	perm = array('i', [i for i in range(n)])
	# group and t are required!!
	def compareUsing2T(a, b):
		if group[a] != group[b]: return group[a]-group[b]
		return group[a+t] - group[b+t]
	while t < n:
		perm = sorted(perm, key = cmp_to_key(compareUsing2T))
		newGroup = array('i', [0 for i in range(n+1)])
		newGroup[n] = -1
		newGroup[perm[0]] = 0
		for i in range(1, n):
			if compareUsing2T(perm[i-1], perm[i])<0:
				newGroup[perm[i]] = newGroup[perm[i-1]] + 1
			else:
				newGroup[perm[i]] = newGroup[perm[i-1]]
		group = newGroup
		t*=2
	return perm

def getSuffiaxArrayNaive(s):
	perm = array('I', [i for i in range(len(s))])
	perm = array('I', sorted(perm, key=lambda x:s[x:]))
	# perm = [i for i in range(len(s))]
	# perm = sorted(perm, key=lambda x:s[x:])
	return perm

def findStr(s, target):
	# suffixArr = getSuffiaxArrayNaive(s)
	suffixArr = getSuffixArray(s)
	# for i in range(len(s)):
	# 	print s[suffixArr[i]:]
	# print
	retList = []
	findStrHelper(s, target, suffixArr, 0, len(s)-1, retList)
	print retList
	print [suffixArr[i] for i in retList]
	for i in retList:
		print s[suffixArr[i]:suffixArr[i]+10]
	
def findStrHelper(s, target, suffixArr, lo, hi, ret):
	if lo<=hi:
		mid = (lo+hi) // 2
		if equalStartSubStr(s[suffixArr[mid]:], target):
			ret.append(mid)
			findStrHelper(s, target, suffixArr, lo, mid-1, ret)
			findStrHelper(s, target, suffixArr, mid+1, hi, ret)
			return
		elif lo==hi:
			pass
		elif target < s[suffixArr[mid]:]:
			findStrHelper(s, target, suffixArr, lo, mid-1, ret)
		elif target > s[suffixArr[mid]:]:
			findStrHelper(s, target, suffixArr, mid+1, hi, ret)

def equalStartSubStr(big, small):
	if len(big) < len(small): return False
	ret = True
	for eachInd, eachCharInSmall in enumerate(small):
		if big[eachInd] != eachCharInSmall:
			ret = False
			break
	return ret

s = 'mississipi'
s = 'alohomora'
# for i in range(120000):
# 	each = random.choice(string.ascii_lowercase)
# 	s+=each
findStr(s, 'homo')
