import sys, math
from array import array

MAX_N = pow(2, 20)-1
sieve = array('B', [255 for i in range((MAX_N+7) // 8)])
def isPrime(k):
	return (sieve[k >> 3]) & (1 << (k&7))
def setComposite(k):
	sieve[k>>3] &= ~(1 << (k & 7))

def eratosthenes():
	setComposite(0)
	setComposite(1)
	sqrtn = int(math.sqrt(MAX_N))
	for i in range(2, sqrtn+1):
		if isPrime(i):
			for j in range(i*i, MAX_N+1, i):
				setComposite(j)
	for i in range(2, 50):
		if isPrime(i):
			print (i)

eratosthenes()
print (len(sieve) * sieve.itemsize)