import random
import copy
def findKth(nums, k):
	if len(set(nums))==1: return nums[0]
	pivot = random.choice(nums)
	L, R = split(nums, pivot)
	print nums, pivot, L, R, k
	if k<len(L)+1: return findKth(L, k)
	if k>=len(L)+1: return findKth(R, k-len(L))

def split(nums, pivot):
	leftWall = -1
	# res = copy.copy(nums)
	res = nums
	for i in xrange(len(res)):
		if res[i] <= pivot:
			leftWall+=1
			res[i], res[leftWall] = res[leftWall], res[i]
	L, R = res[:leftWall+1], res[leftWall+1:]
	return L,R

nums = [3,1,5,7,6,1,3]
k=4
print findKth(nums, k)
import heapq
print heapq.nsmallest(k, nums)
