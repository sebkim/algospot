import math
def factorSimple(n):
	sqrtn = int(math.sqrt(n))
	ret = []
	for div in range(2, sqrtn+1):
		while (n%div) == 0:
			n/=div;
			ret.append(div)
	if n>1: ret.append(n)
	return ret

for i in range(20):
	print factorSimple(i), i