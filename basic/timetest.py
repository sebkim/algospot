from array import array
from timeit import timeit
import sys, os

def test():
	iterTime = 10000
	x = array('b', [0 for i in range(iterTime)])
	# x = [0.1 for i in range(iterTime)]
	temp = sys.stdout
	# sys.stdout = open(os.devnull, 'w')
	for i in x:
		i+1
	sys.stdout = temp

if __name__ == '__main__':
	print timeit('test()', setup='from __main__ import test', number=10000)