def printPermu(n, picked, toPick):
	if toPick==0:
		print picked
		return
	cand = set(range(n)) - set(picked)
	cand = sorted(cand)
	for eachCand in cand:
		picked.append(eachCand)
		printPermu(n, picked, toPick-1)
		picked.pop()

printPermu(5, [], 2)
print
