def pick(n, picked, toPick):
	if toPick==0:
		print picked
		return
	if len(picked)==0:
		smallest = 0
	else:
		smallest = picked[-1] + 1
	for i in range(smallest, n):
		picked.append(i)
		pick(n, picked, toPick-1)
		picked.pop()

def combi(n, picked, toPick):
	hereSum = 0
	if toPick==0:
		return 1
	if len(picked)==0:
		smallest = 0
	else:
		smallest = picked[-1] + 1
	for i in range(smallest, n):
		picked.append(i)
		hereSum += combi(n, picked, toPick-1)
		picked.pop()
	return hereSum

pick(5, [], 3)
print
print combi(5, [], 3)
