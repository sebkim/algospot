import math
MAX_N = pow(2,22) - 1
isPrime = [True for i in range(MAX_N+1)]
def eratoathenes():
	sqrtn = int(math.sqrt(MAX_N))
	isPrime[0] = isPrime[1] = False
	for i in range(2, sqrtn+1):
		if isPrime[i]:
			for j in range(i*i, MAX_N+1, i):
				isPrime[j] = False

eratoathenes()
for i in range(2, 80):
	if isPrime[i]:
		print i