import math
MAX_N = pow(2,20) - 1
minFactor = [i for i in range(MAX_N+1)]
def eratoathenes():
	minFactor[0] = minFactor[1] = -1
	sqrtn = int(math.sqrt(MAX_N))
	for i in range(2, sqrtn+1):
		if minFactor[i] == i:
			for j in range(i*i, MAX_N+1, i):
				if minFactor[j] == j:
					minFactor[j] = i

def factor(n):
	sqrtn = int(math.sqrt(n))
	ret = []
	while n>1:
		ret.append(minFactor[n])
		n/=minFactor[n]
	return ret

eratoathenes()
print zip(minFactor[:30], [iInd for iInd, i in enumerate(minFactor[:30])])
for i in range(20):
	print factor(i), i
