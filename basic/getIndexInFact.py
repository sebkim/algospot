import operator as op
import copy

def fact(n):
	return reduce(op.mul, xrange(n, 0, -1))

def perm(n, partRet, retList):
	if len(partRet) == n:
		retList.append(copy.copy(partRet))
		return
	for next in xrange(1, n+1):
		if next not in partRet:
			partRet.append(next)
			perm(n, partRet, retList)
			partRet.pop()
retPerm = []
perm(4, [], retPerm)
# print retPerm

factorials = []
def getFactorials(n):
	ret = 1
	factorials.append(1) # for 0!
	for i in range(1, n+1):
		ret *= i
		factorials.append(ret)
getFactorials(5)

# X is some permutation
def getIndex(X):
	ret = 0
	for i in xrange(len(X)):
		less = 0
		for j in xrange(i+1, len(X)):
			if X[i] > X[j]:
				less+=1
		ret += factorials[len(X)-i-1] * less
	return ret

print retPerm
print getIndex([1,4,3,2])
