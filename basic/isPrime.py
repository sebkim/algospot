import math
def isPrime(n):
	if n<=1: return False
	if n==2: return True
	if (n%2)==0: return False
	sqrtn = int(math.sqrt(n))
	for div in range(3, sqrtn+1):
		if (n % div) ==0: return False
	return True

for i in range(1, 20):
	if isPrime(i):
		print i