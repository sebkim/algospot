from collections import deque
def merge_sort(nums, lo, hi):
    if lo<hi:
        middle = (lo+hi)//2
        merge_sort(nums, lo, middle)
        merge_sort(nums, middle+1, hi)
        merge(nums, lo, middle, hi)
def merge(nums, lo, middle, hi):
    buffer1 = deque(nums[lo:middle+1])
    buffer2 = deque(nums[middle+1:hi+1])
    ind = lo
    while not (len(buffer1) == 0 or len(buffer2) == 0):
        if buffer1[0] < buffer2[0]:
            nums[ind] = buffer1.popleft()
        else:
            nums[ind] = buffer2.popleft()
        ind += 1
    while not len(buffer1) == 0:
        nums[ind] = buffer1.popleft()
        ind += 1
    while not len(buffer2) == 0:
        nums[ind] = buffer2.popleft()
        ind += 1

NUMS = [3, 1, 5, 7, 4, 2]
print "before sort: {}".format(NUMS)
merge_sort(NUMS, 0, len(NUMS)-1)
print "after sort: {}".format(NUMS)
