def qsort(nums, lo, hi):
    if lo < hi:
        pivot_location = partition(nums, lo, hi)
        qsort(nums, lo, pivot_location-1)
        qsort(nums, pivot_location+1, hi)

def partition(nums, lo, hi):
    pivot = nums[lo]
    leftwall = lo
    for i in xrange(lo+1, hi+1):
        if nums[i] < pivot:
            leftwall += 1
            nums[i], nums[leftwall] = nums[leftwall], nums[i]
    nums[lo], nums[leftwall] = nums[leftwall], nums[lo]
    return leftwall

NUMS = [3, 1, 5, 7, 4, 2]
print "before sort: {}".format(NUMS)
qsort(NUMS, 0, len(NUMS)-1)
print "after sort: {}".format(NUMS)
