
def sizeGen():
	size=1
	while True:
		yield size
		size *= 2
def printHeap(heap):
	import copy
	copied = copy.copy(heap)
	for size in sizeGen():
		print copied[:size]
		copied = copied[size:]
		if len(copied) == 0:
			break
def pq_parent(p):
	if p==0: return None
	return (p-1) // 2
def pq_young_child(p):
	return (2*p)+1
def bubble_up(heap, p):
	if pq_parent(p) is None:
		return
	if heap[pq_parent(p)] > heap[p]:
		heap[pq_parent(p)], heap[p] = heap[p], heap[pq_parent(p)]
		bubble_up(heap, pq_parent(p))
def bubble_down(heap, p):
	childInd = pq_young_child(p)
	minInd = p
	for i in range(2):
		if childInd+i < len(heap):
			if heap[childInd+i] < heap[minInd]:
				minInd = childInd+i
	if minInd != p:
		heap[minInd], heap[p] = heap[p], heap[minInd]
		bubble_down(heap, minInd)
def heappush(heap, x):
	heap.append(x)
	bubble_up(heap, len(heap)-1)
def heappop(heap):
	if len(heap)<1:
		return None
	else:
		popped = heap[0]
		heap[0] = heap[len(heap)-1]
		heap.pop()
		bubble_down(heap, 0)
		return popped
def heapify(heap):
	for i in range(len(heap)-1, -1, -1):
		bubble_down(heap, i)
def heapsort(aList):
	import copy
	copied = copy.copy(aList)
	heapify(copied)
	ret = []
	for i in range(len(copied)):
		ret.append(heappop(copied))
	return ret
import random
heap = range(10)
random.shuffle(heap)
heapify(heap)
# print heapsort(heap)
print printHeap(heap)
