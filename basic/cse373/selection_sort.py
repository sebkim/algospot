def selection_sort(nums):
    for i in xrange(len(nums)):
        min_val = float('inf')
        for j in xrange(i, len(nums)):
            if nums[j] < min_val:
                min_val = nums[j]
                min_val_pos = j
        nums[i], nums[min_val_pos] = nums[min_val_pos], nums[i]

NUMS = [3, 1, 5, 7, 4, 2]
print "before sort: {}".format(NUMS)
selection_sort(NUMS)
print "after sort: {}".format(NUMS)
