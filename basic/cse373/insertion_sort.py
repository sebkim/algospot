def insertion_sort(nums):
    for i in xrange(1, len(nums)):
        val_to_insert = nums[i]
        hole_position = i
        while hole_position>0 and nums[hole_position-1] > val_to_insert:
            nums[hole_position] = nums[hole_position-1]
            hole_position -= 1
        nums[hole_position] = val_to_insert

NUMS = [3, 1, 5, 7, 4, 2]
print "before sort: {}".format(NUMS)
insertion_sort(NUMS)
print "after sort: {}".format(NUMS)
