def bubble_sort(nums):
    for endInd in xrange(len(nums)-1, -1, -1):
        for ind in xrange(endInd+1-1):
            if nums[ind] > nums[ind+1]:
                nums[ind], nums[ind+1] = nums[ind+1], nums[ind]

NUMS = [3, 1, 5, 7, 4, 2]
print "before sort: {}".format(NUMS)
bubble_sort(NUMS)
print "after sort: {}".format(NUMS)
