class treap(object):
	def __init__(self, key=None):
		import random
		import sys
		self.key = key
		self.size = 1
		self.priority = random.randint(0, sys.maxint)
		self.left = None
		self.right = None
		self.parent = None
		self.keySize = 1
	def setLeft(self, newLeft):
		self.left = newLeft
		self.calcSize()
	def setRight(self, newRight):
		self.right =newRight
		self.calcSize()
	def calcSize(self):
		self.size=1
		if(self.left): self.size += self.left.size
		if(self.right): self.size += self.right.size
	def height(self):
		if self.left:
			leftHeight = self.left.height()
		else:
			leftHeight = 0
		if self.right:
			rightHeight = self.right.height()
		else:
			rightHeight = 0
		return max(leftHeight, rightHeight) +1
	def minInTreap(self):
		node = self
		while node.left:
			node = node.left
		return node.key
	def __str__(self, level=0):
		ret = "\t"*level+repr(self.key)+', '+repr(self.priority)+', ' + repr(self.keySize)
		if self.parent:
			ret += ', '+ repr(self.parent.key)
		ret += '\n'
		if self.left:
			ret += self.left.__str__(level=level+1)
		else:
			ret += "\t"*(level+1) + 'Left None.\n'
		if self.right:
			ret += self.right.__str__(level=level+1)
		else:
			ret += "\t"*(level+1) + 'Right None.\n'
		return ret
	def search(self, x):
		if x < self.key:
			if self.left is None:
				return None
			return self.left.search(x)
		elif x > self.key:
			if self.right is None:
				return None
			return self.right.search(x)
		else:
			return self
	def successor(self):
		if self.right is not None:
			succ = self.right
			while succ.left:
				succ = succ.left
			return succ
		# search up direction
		else:
			curNode = self
			if curNode.parent is None:
				return None
			while curNode.parent is not None and curNode.parent.right == curNode:
				curNode = curNode.parent
			if curNode.parent is not None and curNode.parent.left == curNode:
				return curNode.parent
			else:
				return None
	def predessor(self):
		if self.left is not None:
			succ = self.left
			while succ.right:
				succ = succ.right
			return succ
		# search up direction
		else:
			curNode = self
			if curNode.parent is None:
				return None
			while curNode.parent is not None and curNode.parent.left == curNode:
				curNode = curNode.parent
			if curNode.parent is not None and curNode.parent.right == curNode:
				return curNode.parent
			else:
				return None
	def kth(self, k):
		leftSize = 0
		if self.left:
			leftSize = self.left.size
		if k<= leftSize:
			return self.left.kth(k)
		if k==leftSize+1:
			return self.key
		return self.right.kth(k-leftSize-1)
	def verifyParent(self):
		if self.left:
			self.left.helper()
		self.helper()
		if self.right:
			self.right.helper()
	def helper(self):
		if self.left:
			if self != self.left.parent:
				print 'oh'
		if self.right:
			if self != self.right.parent:
				print 'oh2'

def lower_bound(node, key, lb = None):
	if node:
		if node.key >= key:
			return lower_bound(node.left, key, node)
		else:
			return lower_bound(node.right, key, lb)
	return lb
def upper_bound(node, key, ub = None):
	if node:
		if node.key > key:
			return upper_bound(node.left, key, node)
		else:
			return upper_bound(node.right, key, ub)
	return ub
def countLessThan(node, key):
	if node is None:
		return 0
	if node.key >= key:
		return countLessThan(node.left, key)
	leftSize = node.left.size if node.left else 0
	return leftSize + 1 + countLessThan(node.right, key)

# return NodePair.
# left one is less than key, and right one is equal or greater than key.
def split(root, key):
	if root is None:
		return (None, None)
	if root.key < key:
		nodepair = split(root.right, key)
		root.setRight(nodepair[0])
		if nodepair[0] is not None:
			nodepair[0].parent = root
		return (root, nodepair[1])
	nodepair = split(root.left, key)
	root.setLeft(nodepair[1])
	if nodepair[1] is not None:
		nodepair[1].parent = root
	return (nodepair[0], root)
# return root node after inserting
def insert(root, newNode, parentParam = None):
	if root is None:
		newNode.parent = parentParam
		return newNode
	if newNode.key == root.key:
		root.keySize+=1
		return root
	if root.priority < newNode.priority:
		nodepair = split(root, newNode.key)
		newNode.setLeft(nodepair[0])
		if nodepair[0] is not None:
			nodepair[0].parent = newNode
		newNode.setRight(nodepair[1])
		if nodepair[1] is not None:
			nodepair[1].parent = newNode
		newNode.parent = parentParam
		return newNode
	elif newNode.key < root.key:
		root.setLeft(insert(root.left, newNode, root))
	else:
		root.setRight(insert(root.right, newNode, root))
	return root
# max(root.key) < min(mergedNode.key)
def merge(root, mergedNode):
	if root is None:
		return mergedNode
	if mergedNode is None:
		return root
	if root.priority < mergedNode.priority:
		mer = merge(root, mergedNode.left)
		mergedNode.setLeft(mer)
		if mer:
			mer.parent = mergedNode
		return mergedNode
	mer = merge(root.right, mergedNode)
	root.setRight(mer)
	if mer:
		mer.parent = root
	return root
# return root node after erasing
def erase(root, key):
	if root is None:
		return None
	if root.key == key:
		if root.keySize>1:
			root.keySize-=1
			return root
		retNode = merge(root.left, root.right)
		if retNode:
			retNode.parent = root.parent
		del root
		return retNode
	if key < root.key:
		rootAfterErase= erase(root.left, key)
		root.setLeft(rootAfterErase)
	else:
		rootAfterErase = erase(root.right, key)
		root.setRight(rootAfterErase)
	return root

def matchorder():
	t = None
	for eachKor in korean:
		t = insert(t, treap(eachKor))
	ret = 0
	for eachRuss in russian:
		lower_bound_node = lower_bound(t, eachRuss)
		if lower_bound_node is None:
			t = erase(t, t.minInTreap())
		else:
			ret+=1
			t = erase(t, lower_bound_node.key)
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nPer = int(rl().strip())
	russian = [int(i) for i in rl().strip().split()]
	korean = [int(i) for i in rl().strip().split()]
	print matchorder()

