
def lunchbox():
	beginEat = 0
	ret = 0
	for melt, eat in meltEatTime:
		beginEat+=melt
		ret = max(ret, beginEat + eat)
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nBox = int(rl().strip())
	meltTime = [int(i) for i in rl().strip().split()]
	eatTime = [int(i) for i in rl().strip().split()]
	meltEatTime = zip(meltTime, eatTime)
	meltEatTime = sorted(meltEatTime, key=lambda x:x[1], reverse=True)
	print lunchbox()
	