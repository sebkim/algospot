
def strjoin():
	import heapq
	heap = nEachStr
	heapq.heapify(heap)
	ret = 0
	while len(heap)>1:
		firstMin = heapq.heappop(heap)
		secondMin = heapq.heappop(heap)
		heapq.heappush(heap, firstMin+secondMin)
		ret += firstMin + secondMin
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nStrs = int(rl().strip())
	nEachStr = [int(i) for i in rl().strip().split()]
	print strjoin()
	