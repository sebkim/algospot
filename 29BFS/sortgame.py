
from collections import deque
def precalc():
	seq = range(1, nNum+1)
	seq = tuple(seq)
	q = deque([])
	q.append(seq)
	distance[seq] = 0
	while len(q)!=0:
		seq = q.popleft()
		cost = distance[seq]
		for i in range(nNum-1):
			for j in range(i+1, nNum):
				newSeq = seq[:i] + tuple(reversed(seq[i:j+1])) + seq[j+1:]
				if distance.get(newSeq) is None:
					q.append(newSeq)
					distance[newSeq] = cost+1
def sortgame():
	fixed = []
	for i in range(nNum):
		small = 1
		for j in range(nNum):
			if i!=j and nums[i] > nums[j]:
				small+=1
		fixed.append(small)
	return distance[tuple(fixed)]

import sys, copy
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
distance = {}
for eachCase in range(n):
	nNum = int(rl().strip())
	nums = [int(i) for i in rl().strip().split()]
	precalc()
	print sortgame()
