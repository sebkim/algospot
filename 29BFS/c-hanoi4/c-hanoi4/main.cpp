

#include <iostream>
#include <stdio.h>
#include <vector>
#include <queue>
#include <cmath>

using namespace std;

const int MAX_DISCS = 12;
const int MAX_BARS = 4;
int cost[1<<(2*MAX_DISCS)];
int nDisc;
vector<int> barStat[MAX_BARS];
int endStat;

// return whichBar that it belongs to
int get(int state, int whichDisc)
{
    return (state>>(2*whichDisc)) & (3);
}
// set discs's bar position to whichBar
int set(int state, int whichDisc, int whichBar)
{
    return  (state & (~(3<<(2*whichDisc)))) | (whichBar<<(2*whichDisc));
}
int sgn(int x)
{
    if(!x) return 0; return x>0 ? 1: -1;
}
int incr(int x)
{
    if(x<0) return x-1;
    return x+1;
}

int hanoi4(int discs, int startStat)
{
    if(startStat == endStat) return 0;
    queue<int> q;
    q.push(startStat); cost[startStat] = 1;
    q.push(endStat); cost[endStat] = -1;
    while(!q.empty()) {
        int here = q.front();
        q.pop();
        int top[4] = {-1, -1, -1, -1};
        for(int i=discs-1;i>=0;i--) {
            top[get(here,i)] = i;
        }
        for(int i=0;i<MAX_BARS;i++) {
            if (top[i]!=-1)
                for(int j=0;j<MAX_BARS;j++) {
                    if(i!=j && (top[j]==-1 || top[i] < top[j]) ) {
                        int there = set(here, top[i], j);
                        // if not discovered
                        if(cost[there]==0) {
                            cost[there]=incr(cost[here]);
                            q.push(there);
                        }
                        // if bidirec meets
                        else if ( sgn(cost[here]) != sgn(cost[there]) ) {
                            return abs(cost[here]) + abs(cost[there]) -1;
                        }
                        
                    }
                }
        }
    }
    return -1;
}

int main() {
    ios::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        for(int i=0;i<MAX_BARS;i++) barStat[i].clear();
        cin>>nDisc;
        for(int i=0;i<MAX_BARS;i++) {
            int nDiscInThisBar;
            cin>>nDiscInThisBar;
            for(int j=0;j<nDiscInThisBar;j++) {
                int whichDisc;
                cin>>whichDisc;
                barStat[i].push_back(whichDisc);
            }
        }
        
        int startStat = 0;
        for(int i=0;i<MAX_BARS;i++) {
            for(int j=0;j<barStat[i].size();j++) {
                startStat |= (i << (2*(barStat[i][j]-1)));
            }
        }
        endStat = 0;
        for(int i=0;i<nDisc;i++) {
            endStat |= (3<<(2*i));
        }
        for(int i=0;i<(1<<(2*MAX_DISCS))-1;i++) {
            cost[i]=0;
        }
        cout<<hanoi4(nDisc, startStat)<<"\n";
    }
}
