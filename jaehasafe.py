def getPartialMatch(needle):
	lNeedle = len(needle)
	pi = [0 for i in range(lNeedle)]
	begin = 1; matched = 0;
	while begin + matched < lNeedle:
		if needle[begin + matched] == needle[matched]:
			matched+=1
			pi[begin + matched - 1] = matched
		else:
			if matched == 0:
				begin+=1
			else:
				begin += matched - pi[matched-1]
				matched = pi[matched-1]
	return pi

def KMPsearch(haystack, needle):
	lHay = len(haystack); lNeedle = len(needle);
	ret = []
	pi = getPartialMatch(needle)
	begin = 0; matched = 0;
	while begin <= lHay - lNeedle:
		if matched < lNeedle and haystack[begin + matched] == needle[matched]:
			matched+=1
			if matched == lNeedle:
				ret.append(begin)
		else:
			if matched == 0:
				begin+=1
			else:
				begin += matched - pi[matched-1]
				matched = pi[matched-1]
	return ret

def shift(original, target):
	return KMPsearch(original + original, target)[0]
def jaehasafe(statusList):
	init = statusList[0]
	statusList = statusList[1:]
	ret = 0
	cur = init
	direc = -1
	for eachStat in statusList:
		if direc == 1:
			ret += shift(cur, eachStat)
		else:
			ret += shift(eachStat, cur)
		direc *= -1
		cur = eachStat
	return ret

import sys
from array import array
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nStatus = int(rl().strip())
	statusList = []
	for i in range(nStatus+1):
		oneLine = array('c', [j for j in rl().strip()])
		statusList.append(oneLine)
	print jaehasafe(statusList)
