def getPartialMatch(needle):
	lNeedle = len(needle)
	pi = [0 for i in range(lNeedle)]
	begin = 1; matched = 0;
	while begin + matched < lNeedle:
		if needle[begin + matched] == needle[matched]:
			matched+=1
			pi[begin + matched - 1] = matched
		else:
			if matched == 0:
				begin+=1
			else:
				begin += matched - pi[matched-1]
				matched = pi[matched-1]
	return pi

def getPrefixSuffix(hereStr):
	pi = getPartialMatch(hereStr)
	k=len(hereStr)
	ret = []
	while k>0:
		ret.append(k)
		# print hereStr[:k]
		k = pi[k-1]
	return ret

import sys
rl = lambda: sys.stdin.readline()
father = rl().strip()
mother = rl().strip()
processStr = father + mother
ans = getPrefixSuffix(processStr)
ans = list(reversed(ans))
print ' '.join([str(i) for i in ans])
