# cached and eachWord are global.
def hasWord(y, x, posInWord):
	if y>= ySize or x>= xSize or x<0 or y<0:
		return False
	if cached[posInWord][y][x] is not None:
		return cached[posInWord][y][x]
	if board[y][x] != eachWord[posInWord]:
		cached[posInWord][y][x] = False
		return False
	if len(eachWord[posInWord:])==1:
		cached[posInWord][y][x] = True
		return True
	for i in range(8):
		if hasWord(y+dy[i], x+dx[i], posInWord+1):
			cached[posInWord][y][x] = True
			return True
	cached[posInWord][y][x] = False
	return False

import sys

dy = [-1, -1, -1, 0, 1, 1, 1, 0]
dx = [-1, 0, 1, 1, 1, 0, -1, -1]

rl = lambda: sys.stdin.readline()
n = int(rl())
for i in range(n):
	board = []
	while True:
		boardEachLine = rl().strip()
		try:
			nWord = int(boardEachLine)
			break
		except:
			board.append(boardEachLine)
	wordList = []
	for i2 in range(nWord):
		wordList.append(rl().strip())
	ySize = len(board)
	xSize = len(board[0])
	for eachWord in wordList:
		cached = [[[None for i3 in range(xSize)] for i4 in range(ySize)] for i5 in range(len(eachWord)) ]
		hereStr = eachWord + ' '
		ret = 'NO'
		for yy in range(ySize):
			for xx in range(xSize):
				if hasWord(yy, xx, 0):
					ret = 'YES'
					break
		hereStr += ret
		print hereStr
