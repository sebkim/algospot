linked = [[0, 1, 2],
[3, 7, 9, 11],
[4, 10, 14, 15],
[0, 4, 5, 6, 7],
[6, 7, 8, 10, 12],
[0, 2, 14, 15],
[3, 14, 15],
[4, 5, 7, 14, 15],
[1, 2, 3, 4, 5],
[3, 4, 5, 9, 13]]

def pushSwitch(clocks, swt):
	whichLink = linked[swt]
	for whichClockPush in whichLink:
		clocks[whichClockPush]+=3
		if clocks[whichClockPush] == 15:
			clocks[whichClockPush] = 3
def areAligned(clocks):
	if all( i==12 for i in clocks):
		return True
	return False
def clocksync(clocks, startSwt):
	if startSwt==10:
		if areAligned(clocks):
			return 0
		else:
			return float('inf')
	totalCount = float('inf')
	for howManyPush in range(4):
		totalCount = min(totalCount, howManyPush+clocksync(clocks, startSwt+1))
		pushSwitch(clocks, startSwt)
	return totalCount

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	clocks = [ int(i) for i in rl().strip().split(' ') ]
	answer= clocksync(clocks, 0)
	print answer if answer!=float('inf') else -1

