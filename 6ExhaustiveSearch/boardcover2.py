
def rotate(brick):
	ret = [[None for i in range(len(brick))] for j in range(len(brick[0]))]
	for i in range(len(brick)):
		for j in range(len(brick[0])):
			ret[j][len(brick)-i-1] = brick[i][j]
	ret = [''.join(i) for i in ret]
	return ret
def generateRotations(brick):
	rotations = [ [] for i in range(4) ]
	for rot in range(4):
		originY = -1; originX = -1;
		for i in range(len(brick)):
			for j in range(len(brick[0])):
				if brick[i][j] == '#':
					if originY == -1:
						originY = i; originX = j;
					rotations[rot].append( (i - originY, j - originX) )
		brick = rotate(brick)
	retSet = set()
	for eachRot in rotations:
		retSet |= set([tuple(eachRot)])
	return list(retSet)

# delta +1 means putBrick, and delta -1 means offBrick
# if there is no problem in putting brick, return true. Else, false.
def putBrick(y, x, brick, delta):
	ret = True
	for i in range(len(brick)):
		ny = y + brick[i][0]
		nx = x + brick[i][1]
		if ny >= ySize or nx >= xSize or ny < 0 or nx < 0:
			ret = False
		else:
			covered[ny][nx] += delta
			if covered[ny][nx] >= 2:
				ret = False
	return ret

def upperLimit():
	leftSpace = len([j for i in covered for j in i if j==0])
	return (leftSpace / (len(rotations[0]))) +1

def cover(placed):
	global best
	if best >= placed + upperLimit(): return
	y = -1; x = -1;
	for i in range(ySize):
		for j in range(xSize):
			if covered[i][j]== 0:
				y=i; x=j;
				break
		if y != -1: break
	if y==-1:
		best = max(best, placed)
		return
	for eachRot in rotations:
		if putBrick(y, x, eachRot, +1):
			cover(placed +1)
		putBrick(y, x, eachRot, -1)
	covered[y][x] = 1
	cover(placed)
	covered[y][x] = 0

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	ySize, xSize, brickYsize, brickXsize = [ int(i) for i in rl().strip().split(' ') ]
	board = []
	brick = []
	for i in range(ySize):
		oneLine = rl().strip()
		board.append([i for i in oneLine])
	for i in range(brickYsize):
		oneLine = rl().strip()
		brick.append(oneLine)
	covered = [[None for i in range(xSize)] for j in range(ySize)]
	for yy in range(ySize):
		for xx in range(xSize):
			if board[yy][xx] == '#':
				covered[yy][xx] = 1
			elif board[yy][xx] == '.':
				covered[yy][xx] = 0
	best = -1
	rotations = generateRotations(brick)
	cover(0)
	# print rotations
	print best
