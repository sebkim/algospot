# cached and eachWord are global.
def hasWord(y, x, posInWord):
	if y>= ySize or x>= xSize or x<0 or y<0:
		return 0
	if cached[posInWord][y][x] > 0 :
		return cached[posInWord][y][x]
	if board[y][x] != eachWord[posInWord]:
		cached[posInWord][y][x] = 0
		return 0
	if len(eachWord[posInWord:])==1:
		cached[posInWord][y][x] =1
		return cached[posInWord][y][x]
	found = 0
	for i in range(8):
		howMany = hasWord(y+dy[i], x+dx[i], posInWord+1)
		found += howMany
		if howMany > 0:
			pathPlaceHolder = [None, None]
			pathPlaceHolder[0], pathPlaceHolder[1] = y+dy[i], x+dx[i]
			nextPath[posInWord][y][x].append(pathPlaceHolder)
	if found>0:
		cached[posInWord][y][x] = found
		return found
	else:
		cached[posInWord][y][x] = 0
		return 0

def reconstruct(y, x, posInWord, ret):
	ret.append([y, x, eachWord[posInWord]])
	if len(ret) == len(eachWord):
		print ret
	pathPlaceHolder = nextPath[posInWord][y][x]
	if len(pathPlaceHolder) == 0:
		return
	for eachPath in pathPlaceHolder:
		reconstruct(eachPath[0], eachPath[1], posInWord+1, ret)
		ret.pop()

import sys

dy = [-1, -1, -1, 0, 1, 1, 1, 0]
dx = [-1, 0, 1, 1, 1, 0, -1, -1]

rl = lambda: sys.stdin.readline()
n = int(rl())
for i in range(n):
	board = []
	while True:
		boardEachLine = rl().strip()
		try:
			nWord = int(boardEachLine)
			break
		except:
			board.append(boardEachLine)
	wordList = []
	for i2 in range(nWord):
		wordList.append(rl().strip())
	ySize = len(board)
	xSize = len(board[0])
	for eachWord in wordList:
		count = 0
		cached = [[[0 for i3 in range(xSize)] for i4 in range(ySize)] for i5 in range(len(eachWord)) ]
		nextPath = [[[ [] for i3 in range(xSize)] for i4 in range(ySize)] for i5 in range(len(eachWord)) ]
		hereStr = eachWord + ' '
		ret = 'NO'
		for yy in range(ySize):
			for xx in range(xSize):
				if hasWord(yy, xx, 0)>0:
					pathTracked = []
					ret = 'YES'
					count+=cached[0][yy][xx]
					reconstruct(yy, xx, 0, pathTracked)
					# break
		hereStr += ret
		print hereStr, count
		
