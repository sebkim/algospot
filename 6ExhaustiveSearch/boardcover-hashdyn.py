brickType = [[[0, 0], [1, 0], [0, 1]], 
[[0, 0], [0, 1], [1, 1]],
[[0, 0], [1, 0], [1, 1]],
[[0, 0], [1, 0], [1, -1]]]

# delta +1 means putBrick, and delta -1 means offBrick
# if there is no problem in putting brick, return true. Else, false.
def putBrick(board, y, x, bType, delta):
	ret = True
	for i in range(3):
		ny = y + brickType[bType][i][0]
		nx = x + brickType[bType][i][1]
		if ny >= ySize or nx>= xSize or ny<0 or nx<0:
			ret = False
		else:
			board[ny][nx] += delta
			if board[ny][nx] >= 2:
				ret=False
	return ret

def cover(board):
	fs = frozenset()
	for eachLineInd, eachLine in enumerate(board):
		for eachInd, each in enumerate(eachLine):
			if each==1:
				fs |= frozenset( ((eachLineInd, eachInd), ) )

	if all( [i==1 for j in board for i in j] ):
		cached[fs] = 1
		return 1
	if cached.get(fs) is not None:
		return cached.get(fs)
	totalSum = 0
	boolBreak = False
	for yy in range(ySize):
		for xx in range(xSize):
			if board[yy][xx]==False:
				firstEmptySpot = (yy, xx)
				boolBreak = True
				break
		if boolBreak:
			break
	for eachBType in range(4):
		if putBrick(board, firstEmptySpot[0], firstEmptySpot[1], eachBType, 1):
			totalSum += cover(board)
		putBrick(board, firstEmptySpot[0], firstEmptySpot[1], eachBType, -1)
	cached[fs] = totalSum
	return totalSum

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	ySize, xSize = [ int(i) for i in rl().strip().split(' ') ]
	board = []
	for i in range(ySize):
		oneLine = rl().strip()
		board.append([i for i in oneLine])
	for yy in range(ySize):
		for xx in range(xSize):
			if board[yy][xx] == '#':
				board[yy][xx] = 1
			elif board[yy][xx] == '.':
				board[yy][xx] = 0
	cached = {}

	# fsInit = frozenset()
	# for eachLineInd, eachLine in enumerate(board):
	# 	for eachInd, each in enumerate(eachLine):
	# 		if each==1:
	# 			fsInit |= frozenset( ((eachLineInd, eachInd), ) )

	print cover(board)
