from math import log
# numPer, possiblePair are global.
def countPair(taken):
	if taken == (1<<numPer) - 1:
		cached[taken] = 1
		return 1
	if cached[taken] is not None:
		return cached[taken]
	firstFree = -1
	firstZeroPositionNum = (~taken & (-~taken))
	firstFree = int( log(firstZeroPositionNum, 2) )
	totalSum = 0
	for pairWith in range(firstFree+1, numPer):
		if ( taken & ((1<<pairWith)) ) == 0 and set((firstFree, pairWith)) in possiblePair:
			prevTaken = taken
			taken |= (1<<firstFree); taken |= (1<<pairWith);
			cp = countPair(taken)
			totalSum += cp
			if cp>0:
				nextPath[prevTaken].append([firstFree, pairWith])
			taken &= (~(1<<firstFree)); taken &= (~(1<<pairWith));
	cached[taken] = totalSum
	return totalSum

def reconstruct(taken, reconList):
	if taken == (1<<numPer) -1:
		print reconList
		return
	nextPathPlaceholder = nextPath[taken]
	for next in nextPathPlaceholder:
		nextAsNum = 0 | (1<<next[0])
		nextAsNum |= (1<<next[1])
		reconList.append(next)
		reconstruct(taken | nextAsNum, reconList)
		reconList.pop()

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
answer = []
for eachCase in range(n):
	numPer, numPair = [ int(i) for i in rl().strip().split(' ') ]
	if numPair == 0:
		answer.append(0)
		rl()
		continue
	possiblePairPre = [ int(i) for i in rl().strip().split(' ') ]
	possiblePair = []
	onePair = []
	for eachInd, each in enumerate(possiblePairPre):
		onePair.append(each)
		if eachInd % 2 == 1:
			possiblePair.append(set(onePair))
			onePair = []
	cached = [None for i in range((1<<numPer))]
	nextPath = [ [] for i in range((1<<numPer))]
	print countPair(0)
	reconstruct(0, [])
	print
