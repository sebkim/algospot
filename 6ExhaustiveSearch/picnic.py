# numPer, possiblePair are global.
def countPair(taken):
	if all( i==True for i in taken ):
		return 1
	firstFree = -1
	for i in range(len(taken)):
		if taken[i] == False:
			firstFree = i
			break
	totalSum = 0
	for pairWith in range(firstFree, numPer):
		if not taken[pairWith] and set((firstFree, pairWith)) in possiblePair:
			taken[firstFree] = taken[pairWith] = True
			totalSum += countPair(taken)
			taken[firstFree] = taken[pairWith] = False
	return totalSum

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
answer = []
for eachCase in range(n):
	numPer, numPair = [ int(i) for i in rl().strip().split(' ') ]
	if numPair == 0:
		answer.append(0)
		rl()
		continue
	possiblePairPre = [ int(i) for i in rl().strip().split(' ') ]
	possiblePair = []
	onePair = []
	for eachInd, each in enumerate(possiblePairPre):
		onePair.append(each)
		if eachInd % 2 == 1:
			possiblePair.append(set(onePair))
			onePair = []
	answer.append(countPair([False for i in range(numPer)]))
	
for i in answer:
	print i
