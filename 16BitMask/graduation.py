def countOneInBitmask(numb):
	return sum((1 for i in bin(numb)[2:] if i=='1'))
def graduate(semester, taken):
	# base case
	if countOneInBitmask(taken) >= nRequired:
		return 0
	if semester == nSem: return float('inf')
	if cached[semester][taken] is not None: return cached[semester][taken]
	ret = float('inf')
	# filter already taken course and nextTake only allows courses that are open in this semester
	nextTakePossible = classes[semester] & (~taken)
	# filter courses that do not meet prerequisite
	for eachCourse in range(nCourse):
		if (nextTakePossible & (1<<eachCourse)) and \
		((taken & prerequisite[eachCourse]) != prerequisite[eachCourse]):
			nextTakePossible &= ~(1<<eachCourse)
	# traverse all possible subset
	subset = nextTakePossible
	while subset:
		try:
			if countOneInBitmask(subset) > nMaxCourse:
				continue
			nextRet = graduate(semester+1, taken | subset) + 1
			if ret > nextRet:
				ret = nextRet
				# nextBest[semester][taken] = bin(subset)
		finally:
			subset = ((subset-1) & nextTakePossible)
	# when nothing to choose
	nextRet = graduate(semester+1, taken)
	if ret > nextRet:
		ret = nextRet
		# nextBest[semester][taken] = bin(0)
	cached[semester][taken] = ret
	return ret
def reconstruct(semester, taken):
	if semester==nSem or (nextBest[semester][taken] is None):
		return
	next = nextBest[semester][taken]
	if next=='0b0':
		print next
		reconstruct(semester+1, taken)
		return
	print next
	reconstruct(semester+1, taken | int(next, 2))

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	nCourse, nRequired, nSem, nMaxCourse = [int(i) for i in rl().strip().split()]
	prerequisite = [None for i in range(nCourse)]
	for eachCourse in range(nCourse):
		oneLine = [int(i) for i in rl().strip().split()]
		prerequisite[eachCourse] = sum((pow(2, i) for i in oneLine[1:]))
	classes = [None for i in range(nSem)]
	for eachSem in range(nSem):
		oneLine = [int(i) for i in rl().strip().split()]
		classes[eachSem] = sum((pow(2, i) for i in oneLine[1:]))
	cached = [[None for i in range((1<<nCourse))] for j in range(nSem)]
	# nextBest = [[None for i in range((1<<nCourse))] for j in range(nSem)]
	ans = graduate(0, 0)
	# reconstruct(0, 0)
	if ans != float('inf'): print (ans)
	else: print ('IMPOSSIBLE')
	# print ("")
	