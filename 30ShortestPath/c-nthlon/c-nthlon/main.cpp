

#include <iostream>
#include <stdio.h>
#include <vector>
#include <queue>
#include <cmath>

using namespace std;

const int INF = pow(2, 30);
const int START = 401;
vector<pair<int, int> > adj[402];
vector<int> firAth, secAth;

int vertex(int delta) {
    return delta+200;
}

vector<int> dijkstra(int src) {
    vector<int> dist(402, INF);
    dist[src] = 0;
    priority_queue<pair<int, int> > pq;
    pq.push(make_pair(0, src));
    while(!pq.empty()) {
        int cost = -pq.top().first;
        int here = pq.top().second;
        pq.pop();
        if(dist[here] < cost) continue;
        for(int i=0;i<adj[here].size();i++) {
            int newDist = cost + adj[here][i].second;
            int there = adj[here][i].first;
            if(dist[there] > newDist) {
                dist[there] = newDist;
                pq.push(make_pair(-newDist, there));
            }
        }
    }
    return dist;
}

int main() {
    ios_base::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        int nGame; cin>>nGame;
        firAth = vector<int> (nGame);
        secAth = vector<int> (nGame);
        for(int i=0;i<nGame;i++) {
            cin>>firAth[i]; cin>>secAth[i];
        }
        // make finite graph
        for(int i=0;i<402;i++) {
            adj[i].clear();
        }
        for(int i=0;i<firAth.size();i++) {
            int delta = firAth[i] - secAth[i];
            adj[START].push_back(make_pair(vertex(delta), firAth[i]));
        }
        for(int delta=-200;delta<=200;delta++) {
            for(int i=0;i<firAth.size();i++) {
                int next = delta + firAth[i] - secAth[i];
                if (abs(next)>200) continue;
                adj[vertex(delta)].push_back(make_pair(vertex(next), firAth[i]));
            }
        }
        ////
        vector<int> dist = dijkstra(START);
        int ret=dist[vertex(0)];
        if(ret == INF) cout<<"IMPOSSIBLE\n";
        else cout<<ret<<"\n";
    }
}
