

#include <iostream>
#include <stdio.h>
#include <vector>
#include <queue>
#include <cmath>

using namespace std;

double INF = pow(2, 32);
int nVer, nEdge, nSpot, nFire;
vector<pair<int, int> > *adj;
vector<int> spots;
vector<int> fires;

int firetrucks() {
    vector<double> dist(nVer+1, INF);
    dist[0] = 0;
    priority_queue<pair<int, int> > pq;
    pq.push(make_pair(0, 0));
    while(!pq.empty()) {
        int cost = -pq.top().first;
        int here = pq.top().second;
        pq.pop();
        if(dist[here] < cost) continue;
        for(int i=0;i<adj[here].size();i++) {
            int newDist = cost + adj[here][i].second;
            int there = adj[here][i].first;
            if(dist[there] > newDist) {
                dist[there] = newDist;
                pq.push(make_pair(-newDist, there));
            }
        }
    }
    int ret=0;
    for(int i=0;i<nSpot;i++) {
        ret+=dist[spots[i]];
    }
    return ret;
}

int main() {
    ios_base::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>nVer;
        cin>>nEdge;
        cin>>nSpot;
        cin>>nFire;
        adj = new vector<pair<int, int> >[nVer+1];
        for(int i=0;i<nEdge;i++) {
            int one, two, weight;
            cin>>one; cin>>two; cin>>weight;
            adj[one].push_back(make_pair(two, weight));
            adj[two].push_back(make_pair(one, weight));
        }
        spots = vector<int>(nSpot);
        fires = vector<int>(nFire);
        for(int i=0;i<nSpot;i++) {
            cin>>spots[i];
        }
        for(int i=0;i<nFire;i++) {
            cin>>fires[i];
            adj[0].push_back(make_pair(fires[i], 0));
            adj[fires[i]].push_back(make_pair(0, 0));
        }
        
        cout<<firetrucks()<<"\n";
        
        delete [] adj;
    }
}
