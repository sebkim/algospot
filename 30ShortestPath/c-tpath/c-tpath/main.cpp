
#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <algorithm>
#include <fstream>

using namespace std;

const int INF = 987654321;
int nVer, nEdge;
vector<pair<int, int> > *adj;

class UnionFind {
public:
    vector<int> parent, rank;
    UnionFind(int n) {
        for(int i=0;i<n;i++) {
            parent.push_back(i);
            rank.push_back(1);
        }
    }
    int find(int key) {
        if(parent[key] == key) return key;
        return parent[key] = find(parent[key]);
    }
    bool merge(int a, int b) {
        a=find(a); b=find(b);
        if(a==b) return false;
        if (rank[a] > rank[b]) swap(a,b);
        parent[a] = b;
        if (rank[a] == rank[b]) rank[b]+=1;
        return true;
    }
};

vector<pair<int, pair<int, int>> > sortEdges() {
    vector<pair<int, pair<int, int>> > edges;
    for(int here=0;here<nVer;here++) {
        for(int i=0;i<adj[here].size();i++) {
            int weight = adj[here][i].second;
            int u = here;
            int v = adj[here][i].first;
            edges.push_back(make_pair(weight, make_pair(u, v)));
        }
    }
    sort(edges.begin(), edges.end());
    return edges;
}

int minUpperbound(int low, vector<pair<int, pair<int, int>> > &edges) {
    UnionFind sets(nVer);
    for(int edgeInd=0;edgeInd<edges.size();edgeInd++) {
        int u = edges[edgeInd].second.first;
        int v = edges[edgeInd].second.second;
        int weight = edges[edgeInd].first;
        if(weight < low) continue;
        if(sets.find(u) == sets.find(v)) continue;
        sets.merge(u, v);
        if(sets.find(0) == sets.find(nVer-1)) return weight;
    }
    return INF;
}

int main() {
//    streambuf *cinbuf = cin.rdbuf();
//    ifstream in("/Users/sanari85/github/algospot/test1.txt");
//    cin.rdbuf(in.rdbuf());
    
    ios_base::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>nVer; cin>>nEdge;
        adj = new vector<pair<int,int> >[nVer];
        for(int i=0;i<nEdge;i++) {
            int a,b,c; cin>>a; cin>>b; cin>>c;
            adj[a].push_back(make_pair(b, c));
            adj[b].push_back(make_pair(a, c));
        }
        vector<pair<int, pair<int, int>> > edges = sortEdges();
        int ans=INF;
        for(int edgeInd=0;edgeInd<edges.size();edgeInd+=2) {
            int low = edges[edgeInd].first;
            int high = minUpperbound(low, edges);
            int diff = high - low;
            if(ans > diff) ans = diff;
        }
        delete [] adj;
        cout<<ans<<"\n";
    }
}
