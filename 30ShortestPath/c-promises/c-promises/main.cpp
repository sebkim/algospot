
#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>

using namespace std;

const int INF = 987654321;
int nVer, nCurRoad, nNextRoad;
int curAdj[200][200];
vector<vector<int> > nextRoads;

void floyd() {
    for(int i=0;i<nVer;i++) {
        curAdj[i][i] = 0;
    }
    for(int k=0;k<nVer;k++) {
        for(int i=0;i<nVer;i++) {
            if(curAdj[i][k] == INF) continue;
            for(int j=0;j<nVer;j++) {
                curAdj[i][j] = min(curAdj[i][j], curAdj[i][k] + curAdj[k][j]);
            }
        }
    }
}
bool update(int a, int b, int weight) {
    if(curAdj[a][b] <= weight) return false;
    for(int x=0;x<nVer;x++) {
        for(int y=0;y<nVer;y++) {
            curAdj[x][y] = min(curAdj[x][y],
                min(curAdj[x][a] + weight + curAdj[b][y],
                curAdj[x][b] + weight + curAdj[a][y])
            );
        }
    }
    return true;
}

int main() {
    ios_base::sync_with_stdio(false);
    int cases;
    cin>>cases;
    while(cases--) {
        cin>>nVer; cin>>nCurRoad; cin>>nNextRoad;
        nextRoads = vector<vector<int> >(nNextRoad, vector<int> (3));
        for(int i=0;i<nVer;i++) {
            for(int j=0;j<nVer;j++) {
                curAdj[i][j] = INF;
            }
        }
        for(int i=0;i<nCurRoad;i++) {
            int a,b,weight;
            cin>>a; cin>>b; cin>>weight;
            curAdj[a][b] = weight; curAdj[b][a] = weight;
        }
        for(int i=0;i<nNextRoad;i++) {
            int a,b,weight;
            cin>>a; cin>>b; cin>>weight;
            nextRoads[i][0]=a; nextRoads[i][1]=b; nextRoads[i][2]=weight;
        }
        int ans = 0;
        floyd();
        for(int i=0;i<nNextRoad;i++) {
            if (!update(nextRoads[i][0], nextRoads[i][1], nextRoads[i][2])) {
                ans+=1;
            }
        }
        cout<<ans<<"\n";
    }
}
