
import heapq
def routing(src, dest):
	dist = [float('inf') for i in range(nVer)]
	dist[src] = 0
	pq = []
	heapq.heappush(pq, (0, src))
	while len(pq)!=0:
		cost, here = heapq.heappop(pq)
		if dist[here] < cost: continue
		for there, thereCost in adj[here]:
			newDist = cost + thereCost
			if dist[there] > newDist:
				dist[there] = newDist
				heapq.heappush(pq, (newDist, there))
	return dist[dest]
	# return dist

import sys
import math
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nVer, nEdge = [int(i) for i in rl().strip().split()]
	adj = [[] for i in range(nVer)]
	for line in sys.stdin.readlines():
		oneLine = [int(i) if iInd<2 else math.log(float(i)) for iInd, i in enumerate(line.split())]
		adj[oneLine[0]].append((oneLine[1], oneLine[2]))
		adj[oneLine[1]].append((oneLine[0], oneLine[2]))
	print "{:.10f}".format(pow(math.e, routing(0, nVer-1)))
	# print [pow(math.e, i) for i in routing(0, nVer-1)]
