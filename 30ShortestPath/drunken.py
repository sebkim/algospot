import copy
import sys
INF = sys.maxint

def reconstruct_path(w, u, v, pathList, isFirstBool = True):
    if copyMinDistVia[w][u][v] == None:
        if isFirstBool and copyAdj[w][u][v] == INF:
            return
        pathList.append(u)
        if u!=v:
            pathList.append(v)
        return
    reconstruct_path(w, u, copyMinDistVia[w][u][v], pathList, False)
    pathList.pop()
    reconstruct_path(w, copyMinDistVia[w][u][v], v, pathList, False)

def drunken():
    order = []
    for i in xrange(nodeNum):
        order.append((delay[i], i))
    order.sort()

    # when no via points
    for i in xrange(nodeNum):
        for j in xrange(nodeNum):
            if i==j:
                adj[i][j] = 0
            else:
                pass
            W[i][j] = adj[i][j]

    for k in xrange(nodeNum):
        whichNode = order[k][1]
        for i in xrange(nodeNum):
            if adj[i][whichNode] == INF:
                continue
            for j in xrange(nodeNum):
                newDistance = adj[i][whichNode] + adj[whichNode][j]
                if newDistance < adj[i][j]:
                    adj[i][j] = newDistance
                    # minDistVia[i][j] = whichNode
                newTotalDistance = delay[whichNode] + newDistance
                if newTotalDistance < W[i][j]:
                    W[i][j] = newTotalDistance
                    # via[i][j] = whichNode
        # copyAdj[whichNode] = copy.deepcopy(adj)
        # copyMinDistVia[whichNode] = copy.deepcopy(minDistVia)

def getReachability():
    hereAdj = copy.deepcopy(adj)
    for i in xrange(nodeNum):
        hereAdj[i][i] = 0
    for i in xrange(nodeNum):
        for j in xrange(nodeNum):
            hereAdj[i][j] = 1 if hereAdj[i][j] < INF else 0
    for k in xrange(nodeNum):
        for i in xrange(nodeNum):
            for j in xrange(nodeNum):
                hereAdj[i][j] = hereAdj[i][j] or (hereAdj[i][k] and hereAdj[k][j])
    return hereAdj

rl = lambda: sys.stdin.readline()

nodeNum, edgeNum = [int(i) for i in rl().strip().split()]
delay = [int(i) for i in rl().strip().split()]
adj = [[INF for i in xrange(nodeNum)] for j in xrange(nodeNum)]
for i in xrange(edgeNum):
    oneEdge = [int(i2)-1 if i2Ind<2 else int(i2) for i2Ind, i2 in enumerate(rl().strip().split())]
    adj[oneEdge[0]][oneEdge[1]] = oneEdge[2]
    adj[oneEdge[1]][oneEdge[0]] = oneEdge[2]

# reachability = getReachability()
W = [[None for i in xrange(nodeNum)] for j in xrange(nodeNum)]
# copyAdj = [[[None for i in xrange(nodeNum)] for j in xrange(nodeNum)] for k in xrange(nodeNum)]
# via = [[None for i in xrange(nodeNum)] for j in xrange(nodeNum)]
# minDistVia = [[None for i in xrange(nodeNum)] for j in xrange(nodeNum)]
# copyMinDistVia = [[None for i in xrange(nodeNum)] for j in xrange(nodeNum)]
drunken()

testNum = int(rl())
for i in xrange(testNum):
    start, end = [int(i2)-1 for i2 in rl().strip().split()]
    print W[start][end]
    # print 'through {} vertex.'.format(via[start][end])
    # pathList = []
    # reconstruct_path(via[start][end], start, end, pathList)
    # print 'Path is {}'.format(pathList)
    
    # pathList = []
    # through = 5
    # reconstruct_path(through, start, end, pathList)
    # print 'through {} vertex.'.format(through)
    # print 'Path is {}'.format(pathList)
    # print 'minDistance using (<=) delay nodes: {}'.format(copyAdj[through][start][end])
    # print