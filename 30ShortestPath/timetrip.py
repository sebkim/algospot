def getReachable():
	for i in xrange(nVer):
		reachable[i][i] = 1
	for k in xrange(nVer):
		for i in xrange(nVer):
			if reachable[i][k]==0: continue
			for j in xrange(nVer):
				reachable[i][j] = reachable[i][j] or (reachable[i][k] and reachable[k][j])

def bellman(src, target):
	upper = [float('inf') for i in xrange(nVer)]
	upper[src] = 0
	for itera in xrange(nVer-1):
		for here in xrange(nVer):
			for there, thereCost in adj[here]:
				if upper[there] > upper[here] + thereCost:
					upper[there] = upper[here] + thereCost
	# if relax succeed, there should be a negative cycle.
	for here in xrange(nVer):
		for there, thereCost in adj[here]:
			if upper[there] > upper[here] + thereCost:
				if reachable[src][here] and reachable[here][target]:
					return 'NC'
	return upper[target]

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nVer, nEdge = [int(i) for i in rl().strip().split()]
	adj = [[] for i in xrange(nVer)]
	for i in xrange(nEdge):
		oneLine = [int(i) for i in rl().strip().split()]
		adj[oneLine[0]].append((oneLine[1], oneLine[2]))
	reachable = [[0 for i in xrange(nVer)] for j in xrange(nVer)]
	# make adjMat
	for here in xrange(nVer):
		for there, thereCost in adj[here]:
			reachable[here][there]=1
	##
	getReachable()
	minTrip = bellman(0, 1)
	# for getting maxTrip, invert weight.
	for here in xrange(nVer):
		for i in xrange(len(adj[here])):
			adj[here][i] = (adj[here][i][0], -adj[here][i][1])
	##
	maxTrip = bellman(0, 1)
	if minTrip == 'NC': minTrip = 'INFINITY'
	elif minTrip == float('inf'): minTrip = 'UNREACHABLE'
	if maxTrip == 'NC': maxTrip = 'INFINITY'
	elif maxTrip == float('inf'): maxTrip = 'UNREACHABLE'
	else: maxTrip = -maxTrip
	if minTrip == maxTrip == 'UNREACHABLE':
		print 'UNREACHABLE'
	else:
		print minTrip, maxTrip
