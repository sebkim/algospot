
UNWATCHED  = 0
WATCHED = 1
INSTALLED = 2
def gallery(here):
	global installed
	visited[here] = True
	children = [0, 0, 0]
	for there in adj[here]:
		if visited[there] is False:
			children[gallery(there)]+=1
	if children[UNWATCHED]:
		installed+=1
		return INSTALLED
	if children[INSTALLED]:
		return WATCHED
	return UNWATCHED

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nGal, nHall = [int(i) for i in rl().strip().split()]
	halls = []
	for i in range(nHall):
		halls.append([int(i) for i in rl().strip().split()])
	visited = [False for i in range(nGal)]
	adj = [ [] for i in range(nGal) ]
	for eachHall in halls:
		adj[eachHall[0]].append(eachHall[1])
		adj[eachHall[1]].append(eachHall[0])
	installed = 0
	for eachGal in range(nGal):
		if visited[eachGal] is False:
			if gallery(eachGal)==UNWATCHED:
				installed+=1
	print installed
