class Node(object):
	def __init__(self, val):
		self.val = val
		self.next = None
		self.prev = None

class LinkedList(object):
	def __init__(self):
		self.root = None
		self.lastNode = None
		self.llSize = 0
	# insert at last!!
	def insert(self, newNode):
		self.llSize+=1
		if self.root is None:
			newNode.next = newNode
			newNode.prev= newNode
			self.root = newNode
		else:
			newNode.prev = self.lastNode
			newNode.next = self.lastNode.next
			self.lastNode.next = newNode
			newNode.next.prev = newNode
		self.lastNode = newNode
		return self.lastNode
	def erase(self, thisNode):
		self.llSize-=1
		thisNode.prev.next = thisNode.next
		thisNode.next.prev = thisNode.prev
		if thisNode == self.root:
			if self.llSize==0: self.root = None
			else: self.root = self.root.next
		if thisNode == self.lastNode:
			if self.llSize==0: self.lastNode = None
			else: self.lastNode = self.lastNode.prev
		return thisNode
	def llPrint(self):
		cur = self.root
		for i in range(self.llSize):
			print cur.val
			cur=cur.next
		print 'size is {}'.format(self.llSize)

def make_graph():
	for eachWord in words:
		a = ord(eachWord[0]) - ord('a')
		b = ord(eachWord[-1]) - ord('a')
		outDegree[a] +=1
		inDegree[b] +=1
		adj[a].insert(Node(b))
		edgeNameG[a][b].append(eachWord)
def checkEuler():
	plus1=0; minus1=0;
	for i in range(numAlpha):
		delta = outDegree[i] - inDegree[i]
		if delta<-1 or delta>1: return False
		if delta==1: plus1+=1
		if delta==-1: minus1+=1
	return (plus1==1 and minus1==1) or (plus1==0 and minus1==0)
def getEulerCircuit(here, circuit):
	cur = adj[here]
	while cur.llSize>0:
		thereNode = adj[here].root
		there = thereNode.val
		adj[here].erase(thereNode)
		getEulerCircuit(there, circuit)
	circuit.append(here)
def getEulerTrailOrPath():
	circuit = []
	# first try finding trail
	for i in range(numAlpha):
		if outDegree[i] == inDegree[i]+1:
			getEulerCircuit(i, circuit)
			return circuit, 'TRAIL'
	# then path
	for i in range(numAlpha):
		if outDegree[i]:
			getEulerCircuit(i, circuit)
			return circuit, 'PATH'
	return circuit, 'NONE'
def solve():
	make_graph()
	if checkEuler() is False:
		return "IMPOSSIBLE"
	circuit, eulerType = getEulerTrailOrPath()
	if len(circuit)-1 != nWord: return False
	# print len(circuit), nWord
	# print circuit
	circuit = list(reversed(circuit))
	ret = ""
	for i in range(len(circuit)-1):
		a=circuit[i]; b=circuit[i+1];
		ret+=edgeNameG[a][b][-1]
		edgeNameG[a][b].pop()
		ret+=" "
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nWord = int(rl().strip())
	words = []
	for i in range(nWord):
		words.append(rl().strip())
	numAlpha = 26
	# adjacency list representation
	adj = [ LinkedList() for i in range(numAlpha)]
	inDegree = [ 0 for i in range(numAlpha) ]
	outDegree = [ 0 for i in range(numAlpha) ]
	# edgeName Graph
	edgeNameG = [ [ [] for i in range(numAlpha)] for j in range(numAlpha)]
	print solve()
