
def oneway():
	# fi is the number of case where pSum[] == i
	fi = [0 for i in range(nPer)]
	for each in pSum:
		fi[each] +=1
	ret = 0
	for i in range(nPer):
		ret += fi[i] * (fi[i]-1) // 2
	return ret

def maxBuys():
	ret = [0 for i in range(nBox+1)]
	# prev[s] stores the last index where pSum[] == s
	prev = [None for i in range(nPer)]
	for eachInPSumInd, eachInPSum in enumerate(pSum):
		if eachInPSumInd == 0:
			prev[0] = eachInPSumInd
		else:
			# when i is not used
			ret[eachInPSumInd] = ret[eachInPSumInd-1]
			# when i is used
			loc = prev[eachInPSum]
			if loc is not None:
				ret[eachInPSumInd] = max(ret[eachInPSumInd], 1+ret[loc])
			# prev store
			prev[eachInPSum] = eachInPSumInd
	return ret[-1]

import sys
rl = lambda: sys.stdin.readline()
n = int(rl())
for eachCase in range(n):
	MOD = 20091101
	nBox, nPer = [int(i) for i in rl().strip().split()]
	boxes = [int(i) for i in rl().strip().split()]

	# nBox, nPer = 100000, 100000
	# boxes = range(1, 100001)

	# pSum considers pSum[-1] = 0
	pSum = []
	pSum.append(0)
	cum = 0
	for each in boxes:
		cum+= ( each % nPer)
		pSum.append(cum % nPer)
	ans = [oneway() % MOD, maxBuys()]
	print (" ".join([str(i) for i in ans]))
