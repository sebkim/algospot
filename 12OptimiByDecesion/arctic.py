
from collections import deque
def decesion(upperLimit):
	q = deque([])
	q.append(0)
	discovered = [False for i in range(nBase)]
	discovered[0] = True
	while len(q) != 0:
		here = q.popleft()
		for there in range(0, nBase):
			if discovered[there] is False and dist[here][there] <= upperLimit:
				q.append(there)
				discovered[there] = True
	if all(i==True for i in discovered):
		return True
	else:
		return False

def arctic():
	lo = 0.0; hi = math.sqrt(pow(1000. - 0, 2) + pow(1000. - 0, 2))+1
	for it in range(100):
		mid = (lo+hi) / 2.
		if decesion(mid):
			hi = mid
		else:
			lo = mid
	return hi

import sys
import math
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nBase = int(rl().strip())
	bases = []
	for i in range(nBase):
		oneLine = [float(j) for j in rl().strip().split()]
		bases.append(oneLine)
	# distance precalc
	dist = [[None for i in range(nBase)] for j in range(nBase)]
	for i in range(nBase):
		for j in range(nBase):
			dist[i][j] = math.sqrt(pow( (bases[i][0] - bases[j][0]), 2) + pow( (bases[i][1] - bases[j][1]), 2))
	print "{:.2f}".format(arctic())
