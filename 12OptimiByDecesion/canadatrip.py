
# it checks whether >=k guide board can be found at distance
def decision(dist):
	ans = 0
	for i in range(nCity):
		if dist >= lenWay[i] - beforeMeter[i]:
			ans += ( ( min(dist, lenWay[i]) - (lenWay[i] - beforeMeter[i]) ) / gaps[i] ) +1
	return ans >= k

def canadatrip():
	lo = -1; hi=8030001;
	while lo+1<hi:
		mid = (lo+hi) / 2
		if decision(mid):
			hi = mid
		else:
			lo = mid
	return hi

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nCity, k = [int(i) for i in rl().strip().split()]
	lenWay = []
	beforeMeter = []
	gaps = []
	for i in range(nCity):
		oneLine = [int(i) for i in rl().strip().split()]
		lenWay.append(oneLine[0])
		beforeMeter.append(oneLine[1])
		gaps.append(oneLine[2])
	print canadatrip()
	