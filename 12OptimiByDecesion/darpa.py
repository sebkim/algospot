
# greedily selects possible spot
def decision(spot, lowerLimit):
	limit = 0
	installed = 0
	for eachS in spot:
		if eachS >= limit:
			installed+=1
			limit=eachS + lowerLimit
	if installed>=nRequired:
		return True
	else:
		return False
def darpa(spot, gap):
	lo = 0; hi=241.;
	for it in range(100):
		mid = (lo+hi) / 2.0
		if decision(spot, mid):
			lo = mid
		else:
			hi = mid
	return lo

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nRequired, totalSpot = [int(i) for i in rl().strip().split()]
	spot = [float(i) for i in rl().strip().split()]
	print "{:.2f}".format(darpa(spot, nRequired))
