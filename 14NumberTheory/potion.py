
from math import ceil
def gcd(a, b):
	if b==0:
		return a
	return gcd(b, a%b)
def potion():
	maxX = put[0] / float(recipie[0])
	maxXInd = 0
	for i in range(1, nPotion):
		cand = put[i] / float(recipie[i])
		if maxX < cand:
			maxX = cand
			maxXInd = i
	b = recipie[0]
	for i in range(1, nPotion):
		b = gcd(b, recipie[i])
	a = ceil(put[maxXInd] * b / float(recipie[maxXInd]))
	# we have to make at least one
	if a==0:
		a=b
	ret = []
	for i in range(nPotion):
		ret.append(int ((recipie[i] * a / b) - put[i]) )
	return ret

import sys
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nPotion = int(rl().strip())
	recipie = [int(i) for i in rl().strip().split()]
	put = [int(i) for i in rl().strip().split()]
	print " ".join([str(i) for i in potion()])
	