import math

def eratoathenes():
	minFactor[0] = minFactor[1] = -1
	sqrtn = int(math.sqrt(MAX_N))
	for i in range(2, sqrtn+1):
		if minFactor[i] == i:
			for j in range(i*i, MAX_N+1, i):
				if minFactor[j] == j:
					minFactor[j] = i

def factor(n):
	sqrtn = int(math.sqrt(n))
	ret = []
	while n>1:
		ret.append(minFactor[n])
		n//=minFactor[n]
	return ret

MAX_N = pow(10,7)
minFactor = [i for i in range(MAX_N+1)]
eratoathenes()

import sys
from collections import Counter
import operator as op
from array import array
from functools import reduce
rl = lambda: sys.stdin.readline()
n = int(rl().strip())
for eachCase in range(n):
	nDiv, lo, hi = [int(i) for i in rl().strip().split()]
	ans = 0
	for each in range(lo, hi+1):
		facCounter = Counter(factor(each))
		nFactor = reduce(op.mul, [i+1 for i in facCounter.values()])
		if nFactor== nDiv:
			ans+=1
	print (ans)
